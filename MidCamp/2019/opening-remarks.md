# Opening Remarks

2019-03-21

## Respect

- code of contact
- taking pictures (yellow lanyard = no photos)

## Other info

- thanks to sponsors
- [Jobs](https://midcamp.org/jobs)
- contributors
- organizers

Help improve the navigation for MidCamp.org: https://www.midcamp.org/2019/midcamp-top-tasks

Go to a sponsor table to get lunch voucher. There will be two sessions,
starting 30 minutes apart.

- accessibility: respect the walking lanes (marked with blue tape)
- captioning: we are testing a new voice-to-text system
    - speak up if you find it helpful
    - mid.camp/192

## Contributing

## Social events

## Upcoming events

- DrupalCon
- area meetups
- drupical.com
- Midwest Open Source Alliance (tax exempt org): midwestopnsourcealliance.org
