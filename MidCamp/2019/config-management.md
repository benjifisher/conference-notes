# Level Up Your Drupal 8 Configuration Management

- Scott Weston

- https://www.midcamp.org/sites/default/files/2019-03/Level%20Up.pdf

## Problem statement

How do you manage different environments with different config, and maintain
sanity?

## Scenario

Different environments, with config differences

- Devel, Webprofiler
- Caching
- Robots.txt
- Logging
- Environment indicator
- Content blocks (ignore in all environments)

Make a spreadsheet.

## Tools

- `config_ignore`
- `config_split`
- `config_filter` (back end for the others)
- `chosen` (improved admin UI)
- `webform_config_ignore`

### Hosting

Most hosting companies provide environment variables that tell you which
environment is running.

## The Config MISO

- Management: if it is the same everywhere, use core config system
- Ignore: use when you do not want config import to modify some config
    - can be specific or use wildcards
    - config will still be exported, but it will not be imported
    - Do not ignore `config_ignore` nor `core.extension`.
    - Examples: `block.block.*`, `robots.txt` (?)
- Split: use when there are differences *and* you want to manage with the
  config system.
    - Use environment variables to change which splits are active.
    - Complete: full module config is in the split
    - Conditional: only certain settings are part of the split
- Overrides: set config values in `settings.php`
    - This overrides anything managed by config.
    - use for `robotstxt.settings`
    - use to select active/inactive splits
    - API keys and other secrets

## Solution

- S: Devel, Webprofiler
- S: Caching
- Robots.txt
- S: Logging
- O: Environment indicator
- Content blocks (ignore in all environments)

### Steps

1. Configure ignores
1. Add environment variables switches in `settings.php`
1. Create `config_split` directories (siblings to main sync directory)
1. Create config splits

## Note

See the slides for some technical details on how to set up the config splits.

## Feedback

mid.camp/250

## Q&A

- Q: How does this work with version control?
- A: Really well.

- Q: What if you have an API key in your configuration, outside version
  control?
- A: You can put it in a `local.settings.php`.

- Q: How can you set up config split in an install profile?
- A: I don't know.

- Q: Can you disable a module in one split or do you have to enable it in all
  the others?
- A: I don't know.
