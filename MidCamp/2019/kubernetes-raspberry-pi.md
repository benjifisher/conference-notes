# Everything I know about Kubernetes I learned from a cluster of Raspberry Pis

- Jeff Geerling

- [Session page](https://www.midcamp.org/2019/topic-proposal/everything-i-know-about-kubernetes-i-learned-cluster-raspberry-pis)

- [GitHub repo](https://github.com/geerlingguy/raspberry-pi-dramble)
- [Live site](http://www.pidramble.com/)

# Raspberry Pi

What can I do with it?

How well does Kubernetes run on a cluster of credit-card sized computers?

How well does Drupal 8 run on that cluster?

# Kubernetes

First, what is Kubernetes?

You have applications and services you need to run:

- Drupal
- MySQL
- Solr
- Redis
- Cron
- ...

You have a bunch of servers to run this on (for HA).

Kubernetes puts your stuff on servers and keeps them running.

As a learning exercise, let's deploy a simple blog on Drupal on ...
Lessons learned on the small scale can help when you run into problems on a
larger scale.

Look for pidraml.org - site running on a cluster like this from Jeff's home.

## How

How do you get Kubernetes and Drupal on a cluster of Raspberry Pis?

Jeff wrote some Ansible scripts to install K on RP.

Why Ansible?

- lots of reasons

## Demo

## Home as a data center

- Power supply
- internet connection
- physical security
- ...

## Kubernetes

- still maturing
- need a dictionary
- done well: amazing
- done poorly: failure at web scale

## Q&A

- Q: How about databases
- A: On this cluster, use an SD card. On prod, use a database service

- Q: What if a physical server dies?
- A: "Arc" from heptio creates snapshots for Kubernetes.
