# Actions Speak Louder Than Code

- Fatima Sarah Khalid (@sugaroverflow)

Fatima was in 2nd grade on 9/11/2001. Her best friend broke up with her
because the friend's mother said that Fatima was a terrorist.

Where do I belong? Pakistani, American?

After 9/11, people judge you before they meet you if you are Muslim.

We give up parts of our identity to survive.

Our options and choices are limited by who we are.

Activity: stand and raise your arms ... then reach a little higher

https://app.sli.do/event/yfascktp/login

1. If you are in the exit row of a plane and your neighbor wants you to move,  do
   you stand up for yourself (69%) or cooperate (31%)?
2. On a bus, someone asks what is in your bags. Do you show him or just get
   off the bus?
3. New job, you need a place to pray: storage room, extra conf. room, or ask a
   manager?

## How can we do better?

Belonging:

- tell our stories
- listen to others' stories
- learn and use the language
- do your research
- be mindful
- amplify the unheard voices
- consider your environment
- notice who is missing
- acknowledge that oppression is constant
- you do not have to understand in order to help someone feel safe
- ask "what can I do" or "what do you need" instead of "are you OK"
- choose empathy
- drupaldiversity.com

## Q&A

- Q: Is it getting better or worse?
- A: Sometimes politics makes it worse, but as people talk about these things,
  have the awkward conversations, it gets better.
