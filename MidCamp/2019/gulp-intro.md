# An Introduction to Gulp

- Tim Erickson

## Goals

1. Share some tools
1. Demystify it a bit if you are already using it
1. Have a discussion about FE tools and workflows
1. Let's all learn something new

Gulp is a toolkit for automating painful or time-consuming tasks.

## How I got there

- Drupal 7, Zen theme
    - 7.x-3.x: no SASS
    - 7.x-5.x: SASS with compass
    - 7.x-6.x: SASS with gulp
- Drupal 8, Bootstrap theme
    - SASS or LESS with grunt
- Backdrop
    - no contrib themes with SASS or LESS built in

Starting to work with Backdrop, Tim could not rely on someone else to set up
`gulp` for him.

It does not seem right to ignore all the messages that `gulp` spits out at us.

- https://www.triplo.co/gulp

## What can I do with Gulp?

- compile SASS
- work with chunks of CSS/JS and combine them
- compress CSS/minify JS
- optimize images
- check for style errors
- build a style guide

## Getting started

1. `npm install -g gulp`
1. Create `gulpfile.js`
1. Create `package.json`
1. Install any needed plugins: `npm install [package] --save`

## Gulp tasks

Syntax: `gulp.task('task-name', function() {/* ... */});`

Typically, the function body is something like

`gulp.src(...).pipe(...).pipe(...).pipe(gulp.dest('path'))`.

Default task: `gulp.task('default', 'hello')`

## Watch it!

Use `gulp watch` (and define the `watch` task) in order to run on any file
change.
