# How to Hire and Fire Your Employer

- April Sides

## Why do I care?

Time at work: 34%

## What do you have, want, and need?

Think about your assets: hard and soft.

Ask people you trust to help list your soft assets.

Hard assets help in risk assessment.

Think about your personal values. Think about big, transformative moments in
your life. What values are present in these stories? Choose 5.

Aspirations/Goals

What do you do when you have free time?

Will someone pay you to follow your passions?

Growth trajectory: change agent or force for stability?

Work environment:

- in person vs. remote
- large vs. small
- structure: formal vs. informal
- pace: slow vs. fast (boring vs. burnout)
- distraction: low vs. high
- travel: none vs. lots

## Should I stay or should I go?

Evaluate:

- striving or thriving
- work/life balance
- burnout

Evaluate your employer

- are values aligned
- respect leadership?
- appreciated?
- opportunities for growth?

Do not give notice as a form of salary negotiation. Ask for the raise first.

ABZ career planning:

- A: now
- B: next
- Z: life boat

## How do you find the right employer?

Research

- consult your network
- build your network: play games, have fun, help others
- job search channels
- rating sites (e.g. glassdoor, comparably)
- check web sites of candidate organizations
- apply and interview (not a commitment)
- ask questions that give you more insight: does the candidate align with your
  values?
- contact current or former employees
- filter your research: consider bias ...
- make a spreadsheet

## Evaluate risk

- what is the worst-case scenario? is it bearable?
- can you back out after starting?
- consider age and stage

> If you don't find risk, risk will find you.

10-10-10: how will you feel

- in 10 minutes
- in 10 months
- in 10 years

> If you don't ask, the answer is always no.

Salary negotiation

- easier before you start than later

Negotiate your start date

- consider obligations to current job
- take time off if you can
- express excitement

Time off between jobs

- health insurance
- financial obligations

## How to fire your current employer

Give notice.

Send a letter.

Think about what happens when someone leaves: good-bye party or escorted from
the building?

Fulfill obligations:

- work with manager to transition
- may work as a contractor

Build bridges, do not burn them.

## Give feedback

If you do not have an exit interview, give them a letter.

Make things easier for your coworkers, replacement.

## Final thoughts

- introspect
- network
- apply and negotiate
- ...

## Resources

- _Radical Candor_
- ...

## Q&A

- Q: Do people change jobs too often, esp. in our industry?
- A: Do not leave just because it is hard.

- Q: If I am the most technical person on the team and am thinking of leaving,
  how do I prepare my team?
- A: Documentation: good practice anyway. Groom your replacements.

- Q: How do you make remote work successful?
- A: Think about how much interaction you need/want. Consider a co-working
  space.

- Q: How do you learn about culture during interviews?
- A: It helps if you know someone who works there or did work there.
