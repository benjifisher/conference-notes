# Lightning Talks

## Developer Burnout

- Michael Miles

## Example Migration Project

progressively decoupled project

- https://www.sce.com/

## Soong - a PHP library for ETL migrations, inspired by Drupal

- Mike Ryan

- https://soong-etl.readthedocs.io/en/latest/

## Confessions of a "Deplorable"

- Kevin Finkenbinder

## Tips for Early-Career Developers
