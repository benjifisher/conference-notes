# My MidCamp Volunteer Schedule

[Volunteers Instructions](https://docs.google.com/document/d/1DNSX7Lsbpyvy9yU37kyvVxliafFaV3IdmrBV8SUSH-g/edit#heading=h.7f4gcmtdca05)

1. Registration: Mar 20th 12:00 PM - 1:00 PM
Set up and prepare the venue for the event.

Duties will include:
- Placing signage
- Placing walking lane tape
- Preparing registration tables
Shift notes: Check in people and make sure everyone knows where to go.


2. Room Monitor [314B]: Mar 21st 2:50 PM - 3:20 PM
Shift notes: Everything I know about Kubernetes I learned from a cluster of Raspberry Pis


3. Room Monitor [325]: Mar 21st 3:40 PM - 4:40 PM
Shift notes: #VoiceFirst: Ready Your Content to Serve 50% of Global Searches


4. Set up [DePaul Student Center]: Mar 22nd 7:30 AM - 9:30 AM
Set up and prepare the venue for the event.

Duties will include:
- Placing signage
- Placing walking lane tape
- Preparing registration tables


5. Room Monitor [312]: Mar 22nd 2:25 PM - 3:25 PM
Shift notes: An Introduction to Gulp


6. Room Monitor [314A]: Mar 22nd 3:45 PM - 4:15 PM
Shift notes: How to Hire and Fire Your Employer
