# Federated Search with Drupal, Solr, and React

- Matthew Carmichael (Matt)
- Dan Montgomery

- blog: https://www.palantir.net/blog/introducing-federated-search

## Overview

- React: display results
- Solr: index
- Drupal: connect the two
- Client: U. Michigan

They were using Google Search Appliance, which was EOL'd so they needed a
replacement.

Indexing different sites with Solr is not too hard, but how do you display
results consistently when they have different architecture, different
technology?

Goal: Make it simple (like Google search).

Requirements:

- simple way to store, retrieve data
- cross-platform
- speedy, usable, responsive front end
- flexible, extensible model
- drop-in replacement for deprecated Google products

## Demo

They have a demo repo with a D7 site (Devel generate), a D8 site (Umami) and a
Solr server ... URL of demo repo?

From the blog post: https://github.com/palantirnet/federated-search-demo

OOTB you get

- highlighting of search terms
- identify source site
- default facets

## Three Part Solution

1. Solr
2. Drupal
3. React

### Solr

Solr expects a uniform schema for results across an index.
It can also create new fields on the fly.

Solr can return JSON. This is convenient for working with React.

### React

`solr-faceted-search-react` (on GitHub) was already available; they forked and
customized. Open Source FTW!

They did some design work, user research, designed it to be "skinned".

### Drupal

You could use some other CMS. Drupal is not a hard requirement.

Drupal has the `search_api_federated_solr` module (?) that lets you pull in
the React app and embed it in the page.

In the U. Michigan implementation, each site has a module that sends results
to Solr, and each site has the module for presenting the search results.

For sending results to Solr, use Search API and Search API Solr modules
(available for D7 and D8). These modules provide a way to map local data to
federated search fields. The mappings support Drupal tokens.

Taxonomy: in order to get unified results, add a field to each taxonomy and,
for each term, set a synonym to be used on the federated search.

Drupal provides indexing (sending to Solr), search pages, and search blocks.

## Get Started

- Search API Federated Solr
- Search API Field Map
- Federated Search React (GitHub, Palantir)
- Federated Search Demo: https://github.com/palantirnet/federated-search-demo
- Dev environment:
    - macOS
    - Composer
    - VirtualBox
    - Ansible
    - Vagrant

## Q&A

- Q: Can you show one site by default, but also be able to show other sites?
- A: The latest release (yesterday) includes that option, along with a lot of
  other features. Also support for searching over a range of fields;
  autocomplete; wildcard search

- Q: Clarify relation between Drupal, React, Solr

- Q: Is this just for multiple sites or would you use it for a single site?
- A: Most of it could also be used for a single site?

- Q: Can you index files, e.g. PDFs?
- A: We have not done that.

- Q: What is the scale of the U. Michigan implementation?
- A: On the order of 10K pages, not sure about usage. The React pages are
  cached, so the limiting factor is how much traffic your Solr server can
  handle.

- Q: Have you used other CMSs as source?
- A: Not yet.

- Q: Can Solr handle authenticated content?
- A: Not implemented.

- Q: If you want to make it easy for WordPress to implement this, then you
  could write as much as possible as a PHP library managed by composer.
- A: Yes, but a lot of it is tied to Drupal configuration and existing
  modules.
