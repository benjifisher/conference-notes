# Adopting a Drupal Contrib-First Approach

- Jason Want, Nerdery

- https://www.midcamp.org/2019/topic-proposal/adopting-drupal-contrib-first-approach

## Leverage the Drupal Community Principles

- Prioritize impact
- Better together
- Strive for excellence
- Treat each other with dignity and respect
- Enjoy what you do

## How to do it

- Engage with the community
    - on line (issue queues, Slack)
    - off line
- Partner with consumers (clients)
- Adopt the contrib-first approach in your practice
- Onboarding: include community engagement in your checklist
- Invest: discuss events, issues, security during standups and such
- Communicate: include contribution in SOW, report on contributions
- Collaborate
- Agile: prioritize and maximize value
- Discovery: identify opportunities early in the process
- Contrib vs. Custom
- Decision Matrix to choose
- See "Contributed versus custom code and everything in between" on d.o.

### Decision Matrix

- Factors:
    - Feature parity
    - Time to implement
    - Forward compatibility
    - Maintainablity
    - Extensibility
- Give a weight to each factor
- Score each option 0-10 on each factor

## Benefits

### Benefits for Consumers

- Increase value of deliverable (esp. considering long-tail factors: see the
  Decision Matrix)
- Increase impact
- Increase quality
- Secure solution
- Forward compatible
- Increased extensibility
- Lowering the cost of ownership

### Benefits for individuals

- Purpose and professional growth
- Additional learning opportunities

### Benefits for providers

- increased brand equity and awareness
- lead generation
- training opportunities
- increased retention
- recruitment

## A checklist

- build consensus
- document your approach
- prepare internal onboarding materials
- prepare external onboarding materials
- iterate

## Q&A

- Q: technical comments on proper rendering; code review and pair programming


