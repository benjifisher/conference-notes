# Event Organizers Panel

- J.D. Flynn (MidCamp)
- Michael Anello (Florida Drupal Camp)
- Kevin Thull (MidCamp)
- AmyJune Hineline (BADCamp)
- April Sides (DrupalCamp Asheville)
- Dan Moriarty (TC Drupal Camp)

## Session for sponsors

This was tried just yesterday at MidCamp. The feedback from the sponsors was
pretty positive.

## Getting more "professional"

- more expensive
- need more sponsors

Should we stop calling them camps and start calling them conferences? Is this
a good thing or a bad thing?

At BADCamp, tickets used to be free. We started charging $20 for training, but
the feeback was very negative.

What is the difference between community event and organized conference? Look
at what people want: some want the "hallway track" to network and meet people,
and others want really good sessions. FL DC raised prices this year in
anticipation of lower sponsorship, but then gave away more free passes. Maybe
camp vs. conference is the wrong question to ask.

Who gets the free tickets? Speakers, volunteers, students from the venue,
people who request them.

How do you get your sponsors to re-up?

- Constant communication.
- Offer a discount (time limited) to sponsors who pay early.

## Branding

How do you distinguish your camp?

- MidCamp (briefly MADCamp): mad hatter
- BADCamp: we are free, it is nice to be in Berkeley in October
- Twin Cities: location, location; tie in with usability studies
- Have a consistent event, a logo, use social media
- Asheville: make it a community event, stay for some vacation time with your
  Drupal friends.
- FL DrupalCamp has decreased the number of sessions for each of the last four
  years in order to give more time for networking. Emphasize sessions where
  everyone is together (keynote, lightning session). There is also food all
  the time.

## What led you to become an organizer, and why do you continue?

- AmyJune: it is my job; I like people. Being an event coordinator is a way to
  set an example of contributing in non-code ways.
- Mike Anello: it is good marketing for a trainer, just as contributing to an
  SEO module is a good way to build your SEO business.
- April: empathy for people starting out the way I was when I started; it is
  like a family reunion. I enjoy planning events. I am interested in
  entrepreneurship.
- Dan: I am unselfish. It is a way to give back to the community. It is a way
  to help people I know, and Drupal in the large, to be successful.
- Kevin: Chicago already had a camp. I learned a lot from that and from
  meetups, so I started one in Fox Valley. I saw a need and filled it. I get
  the feel-good payback.

## Thoughts on broadening to include Wordpress or PHP in general?

- BADCamp brought in Gatsby, which helped a little to reverse decline in
  attendance. It is tricky to expand to other topics.
- Twin Cities does not want to lose its Drupal focus.
- There is so much already in Drupal core (composer, Twig, javascript,
  Symfony) and more in related fields (React). It is easy to find speakers for
  these topics. We can include them and keep our Drupal focus.
- We have talked about how to include Wordpress without actually merging.

## What sort of outreach do you do? Coder camps, related technologies, ...

- AmyJune has been going to local meetups, promoting BADCamp. Gatsby,
  Wordpress, other events.
- April: there are restrictions in the Wordpress model that make it hard to
  work with them.

## Laravel has an on-line conference: cheap, worked pretty well. Has anyone tried that?

- There are a bunch of local meetups in FL, and a lot of them have stopped
  meeting regularly. We are thinking of organizing a state-wide Zoom meetup,
  hosted each month by a different local group. It will be in person from the
  host group, with others participating virtually. We will see how well it
  works.
- The SF users group gets 30-40 people at each monthly meetup. They also have
  some on-line participation. About 4-5 people per meeting.

## What resources do we need for conflict resolution?

Checklist, guide, process? What else?

- training
- get the same sort of crisis intervention training that EMTs receive
