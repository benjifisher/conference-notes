# Digital Well-Being: If you need to resort to alert fatigue, you’re designing it wrong

- Min Jin Kim
- Neha Bhomia
- Danny Teng
- Benjamin Sutton

Alert fatigue leads to **slower reactions**.

Dark UX patterns: e.g., Facebook (trying to get your attention, entice you to
engage).

Activity: find out how many notifications you have received in the last week.

iOS has a setting to limit your time in each app.

Automatic notifications from Jira or other ticket management systems ...

Slack does a better job of sending notifications without overwhelming users.

Facebook is trying to do better. They have added a time-management dashboard.

Companies are responding to push-back, starting to design for better mental
wellness.

As a UX designer, try to provide notifications that your users want and do not
overwhelm them.

## Q&A

- Q: What can I do with Jira to provide better defaults for new accounts?
- A: Not sure. From the audience: it depends on whether you have self-hosted
  Jira.

- Q: Can you filter the alerts on your phone so that you only get the actual
  urgent ones?
- A: Amber alterts are federal, so you cannot stop them. You can allow
  text-message alerts from specific senders even when you have turned on "Do
  Not Disturb" mode.

- Q: What will happen if app developers do not improve?
- A: We will all suffer from alert fatigue, and we will not respond to actual
  emergencies.

- Q: Are there OS solutions to limit notifications?
- A: You can configure for each app how persistent the notifications are.

- Q: Can you limit screen time?
- A: You can use parental controls. You do not want the phone manufacturer to
  make that decision for you.
