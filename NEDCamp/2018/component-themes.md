# Maintainable Drupal 8 Themes Using Components

Kathy Beck (@kbeck303)

## Documentation

Project Documentation
Inline Comments

What, Why, and How to work with it

## Variables, Functions, and Mixins

## Macros, Includes, and Extends

> Make things easier for yourself and your team

## View Modes

Use view modes for everything.

Certainly use view modes with Views.

## Media Entities

> This allows you to manage media in one place.

## Using the systems you work with

(Eat your own dogfood or drink your own champagne.)
