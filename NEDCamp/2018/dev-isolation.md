# Developer Isolation: How DevOps helps team building

Matt Westgate (@metamatt)
Lullabot CEO (Tugboat.qa, drupalize.me)

Collaboration happens when people feel safe and included.

This is based on research, not just anecdotes.

## Collaboration

Information flow:

- pathological (power oriented)
    failure -> scapegoating
- bureaucratic (rule oriented)
    failure -> justice
- generative (performance oriented)
    failure -> inquiry

## Safety

Team effectiveness

Psychological Safety

Every time we withhold information, we rob ourselves and our team of learning
and innovation.

You will not be punished or humiliated for speaking up with ideas, questions,
concerns, or mistakes.

How to get there:

- Frame the work as a learning problem, not an execution problem.
- Acknowledge your own fallibility.
- Model curiosity.

## Final thoughts on Collaboration and Safety

Change usually comes from leadership.

Sometimes, leadership has to change.

It matters more how well the team works together than who is on the team.

More communication leads to less isolation. So make it safe to communicate.

## DevOps

Tooling

Collaborative DevOps tooling.

### Version control

### Code reviews on pull requests

### Preview environments

### Self-service infrastructure

Apply best development practices to your infrastructure.

### Automate, together

Map out the payoff and then measure it.

> When you are on a team that has empathy, trust, and openness, DevOps tends
> to follow. Pizza also helps.

## Q&A

Q: I like to automate everything. But then it becomes harder when something
breaks. How do you get enough people with expertise to fix it?

A: Pay attention to the payback. Automate together. Document. Plan: what
happens if this tool breaks? Do not automate ATT.

Q: Some of the benefit of code review is that it spreads the responsibility,
makes blameless post-mortems easier.

A: Yes

Q: Is there a flow from collaboration to DevOps, or is there a feedback loop?

A: No. DevOps is all about getting changes to prod faster. That can only
happen when people feel safe and collaborate with each other.

Q: How do you handle teams with introverts/extroverts?

A: I don't know. Talk about it.

Q: How do you sell this?

A: This is how the world is moving. You can point to other companies that are
already improving DevOps and the results they get.

Q: How do you train the client on the DevOps culture?

A: Lead by example. Be open, transparent. Put your dev team front and center,
and get out of the way.

Closing thoughts:

- Collaborate openly
- Default to trust
