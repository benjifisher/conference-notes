# An introduction to Static Site Generators for Drupalists

Brian Perry ([bounteous](https://www.bounteous.com/), formerly HS2 Solutions)

bit.ly/ssg-drupal (PDF download)

## Look at some examples

- 1 HTML page
- React docs site
- interactive Gatsby-driven site.

## Static Site Generator

- Generates a static site
- Often based on markdown but could be database, API, ...
- typically uses a templating engine
- End result can be hosted anywhere, cheaply
- Popular:
    - Jekyll
    - Hugo
    - Hexo
    - Gatsby
    - ...

Data -> Static Site Generator -> Hosting

Hosting can be CDN, GitHub pages, ...

Advantages:

- speed
- security
- hosting (simpler, easier to scale)
- developer experience (DX)

But ... I love Drupal. Why would I want a static site?

- Avoid caching hell
    - Think of all the things we already use: Varnish, memcache, Drupal
      internal cache
- next security update
- hosting costs
- Drupal's admin UI

## Static Site Generator Landscape (from a Drupal perspective)

so ... many ... options

### Jekyll

Granddaddy of modern static site generators.

- Ruby
- Liquid templating engine
- Markdown source (or HTML)
- Content in version control
- Supported natively by GitHub Pages
- Incremental builds
- Hugely popular

Incremental builds: when only one or a few source pages change, it can update
just what has changed.

#### Jekyll and Drupal

- There are importers for D6 and D7
- Maybe not for D8?
- options:
    - custom process to export data to markdown
    - Drupal -> RSS -> Markdown -> Jekyll
    - Drupal -> CSV -> Markdown -> Jekyll

#### Selling points

- simple, tested
- you like Markdown
- GitHub Pages matters
- You don't hate Ruby

#### Resources

- docs
- new Mediacurrent.com: adventures in decoupled Drupal

#### Similar projects

- Hexo
- Hugo

### Gatsby

- React project
- JSX Templating
- GraphQL to query data
- Thriving plugin ecosystem
- The new hotness
- Crazy fast. Seriously.
- Not just for static sites

Since it is based on React, it can be used for single-page apps, or anything
else you can do with React.

#### Data sources

- You can write React components in `source/pages`.
- You can have Markdown files.
- Construct queries using GraphQL

#### Templating

- JSX mixed in with React

#### Working with Drupal

- `gatsby-source-drupal`: pulls data from Drupal with JSON API module

#### Selling points

- performance matters
- You think in React.
- You want data source flexibility.
- You want to leverage GraphQL.
- You like shiny, new things.
- You want to go beyond static.

#### Resources

- Gatsby Docs
- Understand limitations of the Drupal source
- Tons of talks and tutorials

#### Similar projects

- VuewPress
- Next.js
- Static Site Generator Webpack plugin

### Tome

- A Drupal project
- Generates a static site that looks like your Drupal site.
- Exports/imports data as JSON

In the works: split out export and import functions.

You can use Drupal -> Tome -> JSON -> Gatsby

#### Selling points

- You want to keep using Drupal tools
- You want Drupal content under version control
- You want to import Drupal into another system

#### Resources

- Tome docs
- Try it.

#### Similar projects

- `static_generator`
- `default_content`
- `yaml_content`

### Automating Deployments

Trigger a build with

- a web hook
- a commit

### Challenges

> But my site cannot be static!

- Content volume / build times
- Preview experience
- Alternative solutions exist for
    - Form API
    - Authentication
- Politics

Challenge yourself:

> Why can't this site be static?

## Q&A

Q: If you need some dynamic pieces, can you fill them in with API calls?

A: Yes, that works. Gatsby probably handles this best.

Q: Some clients have security concerns. They already publish to a restricted
staging site, then push to production.

A: Yes.

Q: How do you avoid getting static versions of your admin pages?

A: I don't know.

Q: Does the Drupal source plugin for Gatsby actually work?

A: Not with Gatsby v2. It needs to be updated.

Q: What is the selling point of using a static version and not just use Drupal
caching and Varnish?

A: You need the whole LAMP stack. Pages are not cached until they are loaded
once.

Q: It is kind of a decoupled front end.

A: Yes.
