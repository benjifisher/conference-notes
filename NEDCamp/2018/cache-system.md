# The Promises and Pitfalls of Drupal’s Cache System

Kelly Lucas (Pega Systems)

## Promises

- complete page caching for anonymous users
- dynamic page caching for authenticated users
- component-level caching for both

## Anonymous Page Caching

D7:

- it was possible, but it was not clever about dependencies

D8:

- Granular invalidation
- Better CDN integration

## Dynamic page caching

If most of a page can be cached, then the expensive part can be built
separately and loaded with the "Big Pipe" strategy.

There is also a separate, more granular, render cache.

## Cacheability Metadata

- Context determine variants
    - route
    - current user
    - ...
- Tags define dependencies
    - node 1
    - media 5
    - user 7
- Max-age
    - 0, not cacheable
    - > 0, cacheable
    - "permanent"

## Auto placeholdering and single flush

- Request
    - First request: High cardinality elements are placeholdered and much of
      the response is cached.
    - N+! request: Partial response is retrieved from the cache
- Placeholders are replaced
- Entire page is flushed (sent to the browser)

## Auto placeholdering and Big Pipe

- Request
    - First request: High cardinality elements are placeholdered and much of
      the response is cached.
    - N+! request: Partial response is retrieved from the cache
- Partial response is flushed (sent to the browser)
- Subsequent flushes send JS commands to replace

## Big Pipe Demo

Use Umami demo install and Big Pipe Demo module.

## Pitfalls

### Customizations

Custom block: you have to render the entire content of a custom block in order
to include the cache data. If you have some fields in a custom block type, and
have a custom template to render it, then do one of the following:

- render the entire `content` in the template
- use the `without` Twig filter to remove the parts you do not want
- use a custom view mode

Context/Tag awareness:

If you add user info (e.g., name) to a custom block, then you have to add
cache contexts or else you will get default caching (per role).

### Lists and newness

How do you correctly invalidate a "most recent activity" view?

By default, every node view is invalidated whenever any node is added or
changed.

One option: install
[Views Custom Cache Tags](https://www.drupal.org/project/views_custom_cache_tag)
module.
Then add custom code that invalidates the custom cache tags.

There is also
[Handy cache tags](https://www.drupal.org/project/handy_cache_tags).

### Automated processes

- Custom imports
- Recurring migrations

Implement incremental updates.

### Big cache but high miss rate

### The "Drupal way"

- Create your dynamic content
- Use correct cache tags/contexts (or add custom ones)
- ...

### Big Pipe support

- Host needs to support output buffering
- CDN (if any) needs to support output buffering
- Beware of bad Drupal behaviors (in the JS sense)

### Unsupported bits in core

Book module: navigation sets `cache-age = 0`

Entity breadcrumb does not update when entity title changes

### Metrics

- CDN high level
- Drupal internals??
- No D8 port of Heisencache module

### Conclusion

- Definitely use caching
- It is complicated
- Tuning takes work and maintenance
