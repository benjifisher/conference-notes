# Meta and Schema: Defining the content about your content

- Jim Birch (@thejimbirch) (Kanopi)

https://jimbir.ch/presentations/meta-schema-drupal/#/

(I introduced myself to my neighbor, Catherine (sp?). She mostly does WP, goes
to Boston Drupal meetup.)

## Our web site vs. syndication

How does our content look when other web sites?
E.g., Google handles 90% of search and shows results on their site instead of
just a link.

What if the platform (e.g. voice controlled) wants only one answer instead of
the top 10?

Get in the game with structured markup.

Specs (each has a validator):

## W3C HTML 5.2 specs

### 4.2 Document metadata:

- head
- title
- base
- link
- style
- script
- meta (`name` and `value` attributes)

## WHATWG Meta Expectations

(Web Hypertext Application Technology Working Group)

More agile than W3C, accept applications for new terms.

## Custom meta tags

Only do this if you have some tool that will use it.

## Validators

https://www.w3.org/developers/tools/

You can look up the meta tags that Google understands.

## Open Graph (OG) Protocol

Introduced by Faceboook, used by LinkedIn, Twitter, ...

http://ogp.me/

### Required

- og:title
- og:type
- og:image
- og:url

Faceboook and Pinterest have debugger/validator

## Twitter cards

- summary card
- summary card with large image
- player card
- app card

There is a validator.

## Implementing tags in Drupal

[Metatag module](https://www.drupal.org/project/metatag)

Do not bother with abstract, keywords ...

Q: How do we know which ones to skip?

There are problems with allowing overrides per node.
It can be overwhelming for content editors and the *long* form can take a long
time to load.

An alternative is to add custom fields, do not allow content editors to
override the metatags.

Problem: creating fallbacks will require custom code.

## Schema (https://schema.org/)

- creative works
- embedded non-text
- event
- health and medical
- organization
- person
- place, ...
- product

### Inline

(`itemprop` and `itemscope` attributes on `div`, `span`, ...)

### RDFa

### JSON-LD (LD = linked data)

This is the one to use. It allows nesting, progressive loading.

### What does Google care about?

Look for search gallery guide in their dev docs.

### Schemas every site needs

- Breadcrumb
- Organization
- Corporate Contact
- Logo
- Social profile (sameAs)
- WebSite
- searchAction

### Content-specific schemas

(lots)

New and beta features are added constantly.

E.g.,

- speakable
- dataset
- FAQs, Q&A, How-To
- Top Places List
- JobPosting

### Keep up

- Google Webmaster Central blog
- [The Keyword](https://www.blog.google/)
- https://pending.schema.org/
- schemaorg issue queue on GitHub

### Do it in Drupal

[Schema.org Metatag](https://www.drupal.org/project/schema_metatag) module

Extends Metatag module.
Covers a lot, easy to extend.
Adds JSON-LD to page header.

In practice, it makes more sense to customize schema for different content
types than to customize other metatags.

## Q&A

Q: What about D7?

A: Schema.org Metatag module was back-ported.

Q: Can you specify breadcrumbs?
A: Maybe you mean quick links. All you can do is ask Google not to show some
    of them.

Q: Where can I get a cheat sheet for which tags matter?
A: [Drupal 8 SEO](https://www.volacci.com/drupal-8-seo-book) book
