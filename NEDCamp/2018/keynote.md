# Real World DevOps

(NEDCamp Keynote)

Jeff Geerling (@geerlingguy)

Author:

- Ansible for DevOps
- Ansible for Kubernetes

Maintainer:

- DrupalVM
- Hosted Apache Solr
- Server Check.in

Family:

wife & 3 kids under 6

Jeff usually does his OS work in 2-3 hours per night, 2-3 nights/week.

## What is DevOps?

Buzzwords:

M$ tries to sell DevOps.

Docker is often cited.

DevOps is in the Cloud.

You have to be Agile.

GitLab:

> People working together ...

Atlassian:

> DevOps help[s] development and operations teams ...

Gartner (and others):

> Rapid IT service delivery ...

Jeff:

> Making people happier while making apps better.

Good DevOps leads to decreased friction between people: developers, testers,
end users, security team, ...

It also leads to a lot less burnout.

There are more frequent code deployments, usually in the middle of the work
day.

It helps to have stable teams.

There are no heroes. No one has to work through weekends.
Not developers, not Ops team.

You have automated tests, CI is good.

Thorough monitoring: server uptime, UX

Fix problems when they happen.

Have a process to manage updates, keep things stable.

Deliver features quickly. Pull requests rarely last more than a couple of
days. Deploy smaller, more frequently.

## Prerequisites

Often quoted:

- Automation
- CI/CD
- Monitoring
- Collaboration

Jeff:

### Easy to make changes

Have you ever timed how long it takes to get a new employee ready to
contribute? (Think PM as well as developer.)

- Easy and fast local dev
- Good automated tests
- Easy rollback

### Easy to fix and prevent problems

Can developers deploy their own changes to production?

- Devs can deploy to prod (after it passes automated tests)
- Detailed monitoring and logging
- Blameless post-mortems

## What about tools?

Avoid having a "golden hammer": a tool that you use for everything, even when
it is not appropriate.

Know when it is worth the effort to learn a new tool, and when it makes sense
to stick with the ones you already have.

### Good tools

- YAGNI
    - You ain't gonna need it.
- Andon Board
    - Toyota: every worker can see any problem in the entire production line
    - Swarm problems. Do not let them fester.
- Time to Drupal
    - 10 minutes for existing developer
    - 1 hour for new developer
- Dev to Prod
    - More than a day: bad
    - Less than an hour: good
- TACT time (time between deployments)

### Conclusion

- better processes
- better communication
- better relationships

### Resources

- The DevOps Handbook
- The Phoenix Project (if you are stuck in bureaucratic hell)
- Refactoring
- The Mythical Man-Month
- The Machine that Changed the World
