# HAX: Headless Authoring Experience

Penn State/ELMS

Web components, ...

- https://haxtheweb.org/
- https://hax.camp/
- https://open-wc.org/
- https://lit-element.polymer-project.org/
- https://www.drupal.org/project/hax

At Penn State, they decided they had to learn a lot of new stuff to move from
Drupal 7 to 8; maybe there are alternatives to learning the D8 theme system.

Web components: finally well supported, make your own tags; work well for
reusing FE assets.

Web components are also good for isolating bits of code: the logo component uses
a lot of JS, and it would be bad if it interfered with other JS on the page.

They just completed a UX study, so there are clear next steps for HAX.

E.g. the NARA (?) logo is a web component; it is an API call.

The HAX editor lets you edit web components. It is sort of like Gutenburg (from
WP) but it does not hack your markup.

A web component can have schema info that HAX can read and figure out how to
edit the component.

If you want to try it out with Drupal, start with the HAX module for Drupal
(link above).

It might be interesting to implement the Media Library upload widget as a web
component. It would be more API-first than Drupal is now: we need to provide an
API endpoint that the web component can call.
