# Progressively Decoupled Drupal, for everyone!

- Bryan Ollendyke
- Rebecca Goodman (National Archives, [NAR](https://www.archives.gov/))
- Chuck Lavera
- Michael Potter

Why decouple?

- clear division of roles
- just makes sense
- keeps Drupal relevant

Problems:

- expensive
- usually requires big teams
- timelines - huh?
- real organiations have many sites on many platforms

ELMS tried React, tried React, threw up hands in frustrastion

## Web components

- four-part browser spec
- create new HTML tags that can be created for specialized uses, and they can be
  (re)used on all platforms
- widely supported and getting better
- widely used
- stop worrying about IE 11

To use a web component, use `<script type="module" ...>` to import the
component.

## National Archives

- first fed. agency using web components
- small team, lots of sites, billions of documents
- convince boss(es): save time and money, future facing
- convince developers: better UX; test installs of various editors

Get started:

- tooling from https://open-wc.org/
- start with one or two components
- do some UX testing

## Using web components over many web sites

Suppose we have designed a nice logo for our brand, and deployed it on hundreds
of sites (say Drupal sites). Now the redesign comes and we have to change the
logo on all those sites.

A web component lives in its own repo. Update in one place, see the effects
everywhere.

- https://www.drupal.org/project/webcomponents (D7 predecessor to HAX module)

Why install the MathJax library for Drupal (or WP or ...)? It is just a JS
library. Create a web component that loads it.

## HAX the web

- HAX CMS
- web components developed for HAX can be used anywhere
