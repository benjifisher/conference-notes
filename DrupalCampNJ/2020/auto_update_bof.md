# Securing Drupal's Auto-Update Infrastructure (BoF)

- Jess (@xjm)
- Neil Drumm
- Peter W
- David Strauss
- Michael Hess
- me

- https://www.drupal.org/project/automatic_updates
- https://www.drupal.org/drupalorg/blog/automatic-updates-phase-1-complete

Original idea:

root key generated off line with Ubi SSM (HSM?) - DS says we generate it on a
laptop, then upload to HSM (4 of them: ND, MH each have one)

There is no way to get the root key back from an HSM. You can copy the key to
another one.

We have established procedures for generating and rotating the intermediate key.
For now, we are rotating monthly, with 45-day expiration. Eventually use 3-month
expiration.

DS wants to switch to [TUF](https://theupdateframework.io/): Python-based (?),
very close to the process we came up with. Or at least consider it. For one
thing, Python script using TUF should be easier than shell script wrapping csig.

DS says that WP has something in the works, but it will not meet the design
criteria that we came up with.

Key signing:
- in-place update artifacts

What if the signing key has expired on the files you are checking with the AU
module (readiness check)?

We can sign composer-included projects because we already build a tarball for
core that includes all of them.
