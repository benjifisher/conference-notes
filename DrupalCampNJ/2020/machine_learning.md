# Machine Learning

## Images on web sites (use case)

Images have lots of advantages, but they present a11y challenges:

- screen readers
- mobile web users
- speech enabled
- SEO

On a daily news web site with LOTS of content, it is too expensive/time
consuming to add alt text to all images. ML is better than gibberish.

## Machine Learning (ML)

Jargon:

- features: numerical rep of data
- classes/labels: final results
- parameters: affect how the alg works

Steps:

- pre--process
- train
- tune
- ...

Some different models:

- Gaussian naive Bayes classifier. E.g. spam filters.
- decision trees
- deep neural network (e.g., Google translate)

scoring and tuning:

- accuracy
- precision
- recall
- F1 score

Depending on the application, false positive or false negative might be more of
a problem.

Ethical concerns:

- biases (authors have biases, and these are transferred to the algs)
- assumptions

... a few war stories

Advantages:

- scale
- efficiency

ML is being used in medical contexts: diagnosis

Challenges:

- difficult to scale locally
- comutationally expensive
- black box

## AWS

- Rekognision (image/video processing)
- Sagemaker
- Comprehend
- Infrastructure

## Drupal module

- [Drupal Rekognition](https://www.drupal.org/project/drupal_rekog)
    Get alt text for images using AWS Rekognition service.
- [Rekognition API](https://www.drupal.org/project/rekognition_api)
    Add taxoomy terms

## Malicious IP addresses (second use case for ML)

spam bots,  DDOS attacks, known bad reputation

Look ar perimeterX if you are willing to pay for protection and need it now.

Dats set: server logs from several large sites

- HTTP status code
- path

We need to label the traffic. You can help! This is the hard part, and where
biases can be transferred.

Volunteer to help:
[ip-data-tagging@umich.edu](mailto:ip-data-tagging@umich.edu).
