# Boston Drupal Meetup 2019-01-23

2/20: Project night
3/20: Looking for a speaker

## Practical Web Accessibility

Spencer Gregson (Wayfair)

https://practical-a11y.now.sh

### What it is

Knowing is half the battle.

Usability: depends on user, task, context.

Requirements vary by client and audience.

How it impacts your site:

- Your audience gets bigger.
- You have to do additional testing (automated and IRL)

### How to do it

If you can't fix it, then don't break it.

Do not abuse HTML elements. Use semantic markup.

Use text alternatives (like `alt` attributes).

Validate your assumptions
and embrace the firehose.

- aXe by the team at deque
    - Chrome plugin
    - Firefox
    - NodeJS CLI
    - Selenium
- pa11y (works with CodeSniffer)
    - CLI
    - CI tool

```
npm install --global pa11y-cli
npx --no-install pa11y-cli https://google.com
npx --no-install pa11y-cli --sitemap http://pa11y.org/sitemap.xml
```

### Preventing regression

Let fixed problems stay fixed.

### Takeaways

Wow, this is really straightforward. At least, getting started is not so bad.

### Q&A

How to collect user feedback?

Add a second "skip link" going to an a11y feedback form.

What about "Read more"?

```html
<a href="/article/preactical-a11y">
  Read more<span class="visually-hidden"> about Practical Accessibility</span>!
</a>
```
