# Drupal and Square

DrupalVM

Generate an SSL cert

If you are using macOS 10.13.{0,1}, then put your shared directories on an HFS
mount.

## Square

You can accept payment in person or on your web site using the Square API, and
you never even have access to the PII that has to be kept confidential.

Goal:  build out a platform for commerce, including PoS, web-based payments
... inventory control, yes; shipping, no

They have specialized "inventory control" systems for restaurants, gyms, etc.

Payment is just the beginning:  invoices, analytics, and more.

### Wep API

Just add a JS file to your page, and it converts HTML with some pre-defined
`#id` attributes into iframes.
The good thing about the iframe is that the customer is communicating with
Square over HTTPS without actually touching your site, so you do not have to
worry about protecting the PII.

Customer enters CC info, Square returns a "nonce" (one-time token), and you
use the nonce to send payment requests to Square.
The API returns some JSON.

Each payment request includes a "idempotency key" to prevent double billing.

You can also use a CC nonce to attach a CC to a customer.
The nonce expires, but you keep track of the customer and Square stores the CC
information.

Another REST request fetches a URL to a payment form. You send the customer to
the URL, they enter payment details there, etc.

## Drupal

Install the Commerce module (with composer).

Install the `commerce_square` module from d.o.:
[Commerce Square Connect](https://www.drupal.org/project/commerce_square)

Sign in to https://squareup.com/developers to get your OAuth token ...

## PoS API

https://docs.connect.squareup.com/

http://tristansokol.github.io/Presentations/
