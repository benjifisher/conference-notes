# Mass.gov: A Guide to Data-Informed Content Optimization

https://events.drupal.org/seattle2019/sessions/massgov-guide-data-informed-content-optimization

- Nathan James (MediaCurrent)
- Julia Gutierrez (Mass.gov)

Think of your most recent "awesome" service experience. That is what we want
to deliver on the Mass.gov sites. Fast, easy, and wicked awesome.

E.g. Buying new eyeglasses.

Data matters in service delivery.

You cannot improve (nor even manage) what you cannot measure.

Collect information to improve mass.gov.
Some numbers ...

How do we collect data and make it actionable?

Each node-edit page has a tab showing data analytics.

Some of that data is also shown in the "My content" view, and there is a list
of just the content that scores poorly.

They also get verbatim copies of the feedback from evaluation forms.

The dashboard gives an overall score, with 4 sub-scores:

- findability
- outcomes
- content quality (grade level, broken links, ...)
- user feedback

Data does not flow. You have to make it move.

- Google tag manager
- GA
- feeback form, formstack
- siteimprove
- Custom Content API (module?)
- ETL: get all these data into a database
- Apache Superset
- Score and Pageview endpoint
- Feedback Manager API

Going beyond a single page, there are many pages connected to a single
service. Look at session quality.

- friction, frustration
- lostness and confusion
- credit/blame ... is it the fault of the page or the IA?

Look at how people navigate the site. If they go back to the home page, or
site search, it is usually not a good sign.

Lessons learned and next steps:

- accuracy vs. scalability: there is a trade-off for each indicator
- best indicators (so far) are about what goes wrong
    - broken links
    - grade level
    - satisfaction score (from feedback form)

How do we measure what is right? We are working on KPIs and dashboards for
them. Working on a configurable KPI measuring system.

- mass.gov
- cross-domain
- omni-channel


## Q&A

- Q: Are you using a single GA account?
- A: Page-level things come from a single account, but there are other
  accounts we are trying to aggregate now.

- Q: Are you looking at other measures, like visual loudness?
- A: Yes ... global and contextual navigation ...
