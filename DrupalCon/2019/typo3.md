# Hello from TYPO3

https://events.drupal.org/seattle2019/sessions/hello-neighboring-island-typo3-cms-postcard-edition

@ekl1773 and @bmack

- 20 people in audience at 12:00

Contribution: how and why; what contributors need

Cooperation: Drupal is now using the TYPO3 PHAR stream wrapper.
There was a lot of cooperation with the Drupal security team.

TYPO3 benefits from tools like Ddev that were developed for Drupal.

Benni's experience getting started with Drupal is that

- there are too many options for setting up
- the "Novice" issues are not novice enough
- uploading patches kinda sucks

Contributing is not just coding: sharing (blogging) is really important

Elli's experience getting started with TYPO3

Good:

- intro package
- good video docs on Youtube
- good community help
- contributing to docs is easy ("edit me on GitHub")
- forget.typo3.com is a visual rep. of issues on forge.typo3.com

Bad:

- some surprises are just the way it works
- some assumptions in docs
- minimalist approach is surprising
- finding an existing issue (before creating a new one) was hard despite not
  having as many issues as Drupal

## Compare and contrast

They have different tooling

Both are
- GPL
- 19 or 20 years old
- using Slack, SO, Twitter

TYPO3 has an all-volunteer assn and also a for-profit company that manages
educational materials and marketing.

## Going forward

We have similar pain points. Let's share.

- Sharing knowledge is key.
- Make contribution a habit.
- Look for simplicity. Make it easier to contribute.

Come to the BoF: bit.ly/TYPO3BOF

## Q&A

- Q: Is there a TYPO2?
- A: There was. The history is that someone accidentally deleted his CMS,
  hence the name TYPO. Version 3 became popular, and part of the name. There
  are newer versions than v3.
