# Dries Note

https://events.drupal.org/seattle2019/driesnote

@hestenet talks about the changes at this year's DrupalCon.

It starts with a marketing video. It shows a lot of happy people, many in
suits, and aims at brand recognition. Personally, I do not recognize anything
in the video.

Shout out to the DDI group. Stats: from 2017 (when we started tracking) to
2019, "underrepresented" speakers increased from about 33% to about 50%.

## Promote Drupal

The Promote Drupal initiative has made progress. There is a brand guide and a
pitch deck.

There is a new format for case studies. (coming soon)

## Drupal.org Tooling

Return of Clippy (4/1)

Phase 1 of the switch to GitLab is done.

Merge requests and in-line editing are coming soon (Q2-Q3 2019).

## Drupal Steward

Coming soon: joint service of Drupal Security Team and DA to operate a Web
Application Firewall. Participating vendors will provide infrastructure-level
mitigation of security vulnerabilities as soon as they are released.

## Thanks

- DA
- event partners
- supporting partners
- individual and organizational contributors
- volunteers

## Housekeeping

- code of conduct
- wifi
- #DrupalCon
- free coffee all day
- lunch 12-2 (WiD in the skybridge)
- Trivia night on Thursday (21+, bring ID)
    - info on PreNote replacement
- group photo following Driesnote

## Aaron Winburn award (CWG)

Our own Leslie Glynn is this year's recipient!

## Intro to Dries (Lynne Capozzi, marketing director at Acquia)

Shout out to Open Pediatrics, a "Drupal application" (distribution?) used at
(I think) Children's Hospital in Boston.

Pitch for Acquia's booth in the exhibit hall ...

There is a new "Acquia Developer Studio" (successor to DevDesktop?) ...
preview available at the booth.

## Dries

Thinking about Drupal Diversity and Inclusion (DDI) group.

Stats for OSS are way worse than for tech in general. Why?

- free time ...  e.g., women still spend more time doing unpaid work at home
- Dries used to say that anyone could contribute to Drupal. All you need is
  interest. In fact, you also need time.
- Supporting D&I is essential to the health of the project.
    - We have a diverse market; it helps to serve them if our contributor base
      is also diverse.
    - Diversity improves collaboration, innovation, creativity
- Intentionally welcome people from under-represented groups
- mentor
- make space (and time)
- Example of supportive code review: do not drive people away.
- Give underrepresented people time to contribute, attend sprints, ...

### Lead by example

We are one of the largest OS communities in the world.

Look at some of the top organizational contributors ...

In the last 6 months ...

- 35% more D8 sites than a year ago
- 48% more stable D8 modules than a year ago
- 3 weeks from now, 8.7 will be released:
    - content creators, site builders:
        - layout builder (demo video)
	    - Isovera is in the slide for orgs that supported the LB.
	- admin UI: targeting 8.8 or 8.9
	    - call for contributors
	- workflow
	- media
    - Make Drupal easy to evaluate and adopt
        - OOTB (finished original goals; continue to improve: accessibility,
	  Spanish translation)
	- Migrate (video interview with @quietone)
    - Keep Drupal relevant and impactful
        - API First
	    - JSON:API is included and stable (leverages Field, Access, Entity
	      APIs)
    - Reduce cost of ownership (developers and site builders)
        - Compare D7/D8 upgrade to D8/D9 upgrade
	- State of Georgia case study for D7/D8 migration
	    - Only work with partners who contribute back to Drupal
	- Pega Systems (highly produced video)
	    - Would you upgrade to Drupal 8 again? The answer is yes, but it
	      is the wrong question, since Pega is still migrating to D8.
	- Why we need to release D9:
	    1. Update our dependencies (esp. Symfony components)
	        - security
		- new features
	    2. Drop deprecated code for maintainability, security, DX
	- D9 = Drupal 8.9 - deprecated code + updated dependencies
	- Any D8 module that does not use deprecated code will work with D9.
	- Deprecation-checking tools: Matt Glaman's `drupal-check` (CLI tool)
	    - Runs as the back end of the `upgrade_status` module
	    - Dwayne tested all contrib modules.
	- Let's remove deprecated code from core, contrib, and custom modules
	- Let's expand the tools to cover Twig and javascript.
	- Drupal 8.8 may deprecate new code, but no more until 9.1
	- Automatic updates: call to action
	    - better Composer support in core and contrib
	    - The EU is providing some funding.
	    - We need an initiative lead.
