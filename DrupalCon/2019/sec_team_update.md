# Updates from the Security Team

https://events.drupal.org/seattle2019/sessions/updates-drupal-security-team

- Michael Hess
- David Snopek
- Catch
- Jess (@xjm)

Who we are ...

What we do ...

We do not ...

- proactively find vulnerabilities
- review maintainers' code
- help fix hacked sites

Workflow ...

Schedule ...

Follow us ...

- d.o user profile has an option
- Twitter
- RSS
- Slack

Other programs:

- D7ES
- Drupal Steward
- D6 extended support
- ...

Drupal 7 EOL

- Security team will no longer support D7
- There are already 2 vendors part of the D7ES program

Good Hygiene (like brushing your teeth)

- It is never done. You cannot just check a list.

Automatic Updates

- It is coming
- Join us on Friday

Drupal Steward

New service from the security team:
a Web Application Firewall (WAF) protecting sites from certain vulnerabilities

The WAF will filter incoming requests with criteria supplied by the security
team.

- At first, only highly critical, mass exploitable core issues will be supported
- Not all vulnerabilities can be mitigated through a WAF
- Available to low-traffic and non-profit sites at close to cost, perhaps $100
  to $250/year.
- It is a service, not code ... it is not free
- The patch is still more effective than the WAF
- When the SA is released, the WAF rules will be made public

Release schedule

- We now provide security coverage for minor releases for a full year.

## Q&A

- Q: How does the WAF work (community tier)?
- A: Still in discussion. Either host-specific or route traffic through a CDN.

- Q: Thanks to the security team!

- Q: What modules should we all use?
- A: seckit, paranoia (D7), ...

- Q: How does a module get security coverage?
- A: Still using the outdated review process ... looking into changes ...

- Q: What about javascript, other systems, and their vulnerabilities?
- A: Do not host your own site unless you are a hosting company.
- A: We coordinate with security teams for Symfony, jQuery, other
  dependencies.

- Q: For contrib modules, how often are they checked?
- A: The security team does not proactively review projects. Full releases of
  covered projects are covered by the security team, but that just means that
  the team will investigate reports ... The module maintainer decides the
  release schedule.

- Q: Is there a good tool for evaluating custom modules?
- A: There are some tools, but code review is best. The Coder module has some
  CodeSniffer rules ... that is one of the tools.

- Q: What about things like the crypt module?
- A: There are 10's of thousands of contrib modules ...
- A: The security team only handles the information that cannot be made
  public. The entire community is responsible for security, including
  security-related modules.
