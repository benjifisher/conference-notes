# A gathering of open source projects discuss evolving their tools

https://events.drupal.org/seattle2019/sessions/gathering-open-source-projects-discuss-evolving-their-tools

- Carlos Soriano, GNOME
- Tim Lehnen, Drupal Association
- Eliran Mesika, GitLab
- Alexander Wirt, Debian

GitLab: two representatives
Red Hat, GNOME: one software engineer

## How have you collaborated with each other in the past?

Debian: tradition of own tools. Over the last 10 years, we have moved from 10
different VC systems to standardizing on Git.

Drupal: For a long time, we were "on an island", and we decided that this was
a problem. Starting with D8, we started to include Symfony and Twig. With
dependencies and composer, we have started to coordinate releases. We would
like to extend our collaboration to community-building.

GNOME: We would like to cooperate more in non-technical areas. We have always
worked with a lot of other projects. GNOME acts as an umbrella organization,
acting as a corporate parent for many others.

## What challenges have you faced with tooling?

Drupal: We are still behind the curve, with a patch-based workflow. When we
migrated from CVS to Git 12 years ago, GitHub and GitLab were not around. The
patch-based workflow is an impediment for new contributors.

GNOME: When I started, 5 years ago, it took me 2 weeks to make my first
contribution. Now, in workshops, we get people to that point in an hour or
two. GitLab was a great partner.

Debian: We decided in 2014 that the old system was not maintainable (fusion
forge, fork of SourceForge). They moved to GitLab, and it took until 2018. We
cannot tell people what to use, we can only encourage them. We now have 30K
repos, 50GB of code.

## Can you give snapshots, then and now?

Drupal: We are still in transition. We are also elf-hosted. Our internal
workflows are important to us. That realization was important to deciding to
make the switch. I think we are still the only major project with issue
credits.

Technically, the transition went incredibly smoothly. The feedback was
uniformly positive. We preserved all the old info, and even links on d.o to
the code were updated to go to the right place.

N.B.: Merge requests are coming "really soon" to d.o.

Debian: MRs and automatic code checks (CI) are a huge improvement.

GNOME: A tool is best when people use it the way it is intended. [My phrasing:
work with the tool instead of fighting against it.] People were happy to jump
in and start using CI and labels. It improved communication between all the
members of the project.

## What do you see as new challenges, opportunities moving forward?

Drupal: We are on a 6-month release cycle. We already moved the infrastructure
over w/o interrupting that, so I think the hard part is behind us. We expect
to see some projects, which moved to GitHub or GitLab, come back to d.o. We
have a home-grown CI system: it works well, is robust, but why should we
continue to maintain it when we can use GitLab CI?

Challenge: maintain our single-threaded conversation model.

Debian: Unify our workflows. We may never switch to the GL issue tracker,
since we like our home-grown system. Projects tend to get slower as they get
older; we hope that the switch to GL will give us a boost.

GitLab: Two goals: Accelerate the contribution process, make it easier for new
contributors

Drupal and GNOME: It is about the health of the project going forward.

GNOME: Permissions and access are the main thing we have to figure out. We are
still working on it. Another challenge is how to enable non-developers to
contribute.

## How hard was it to rally the troops

Drupal: We are consensus-driven. We had several studies, decided that external
tools were not ready. We communicated as much as we could, and the community
supported us.

Debian: After the primary decision, to move to something new, we surveyed our
users. For licensing reasons (CLA?) GitLab was not an option, but negotiations
were successful in changing those restrictions. We work as a do-ocracy. We do
not want to maintain patches to GitLab, so we work to get changes that we need
incorporated into the GitLab codebase.

GNOME: We do not have a good track record with community surveys. We worked in
a small team, made a decision, then opened up the discussion on a Wiki page.

## How did GitLab change as an organization to accommodate these projects?

GitLab is an OS project. We want to support other OS projects, offer our
tooling to help them improve their development.

In all cases, 80% of what they needed was already available OOTB.

With Debian, the problem was the CLA. We adopted their ... and dropped our
CLA.

With GNOME, we already had the fast-forward in our paid version, and moved it
to the OS, free version.

With Drupal, there was an initiative under way to improve memory usage, and we
had to accelerate that so that creating a fork does not mean making a full
copy on disk of the repo.

## Did you have to change processes after moving to GitLab? Bug or feature?

Debian: The only big change was switching to MRs, and there were no serious
pain points.

GNOME: We changed everything. It was a clean break from patch reviews to CI
and MRs. It has been good for us.

Drupal: There are changes coming up. Deciding when to take the plunge and
getting community buy-in were the hard parts.

## Q&A

- Q: Are there "neighboring" projects that inspire you to improve your tools?
- A: ...

- Q: How does the commitment to other OS projects fit into the GitLab business
  model?
- A: It introduces GitLab to a lot of people, and we have already seen
  commercial adoption of GitLab from people working with GNOME. The feature
  requests and direct contributions from these other projects make GitLab a
  better project.

- Q: What other collaborations do you anticipate?
- A: Community building, D&I initiatives, managing security issues, marketing

- Q: What advice do you have for younger projects?
- A: If you can use tooling built elsewhere, do it. In tooling, governance,
  marketing, find tools that let you focus on your project.
- A: Learn to adapt.
- A: Do not let the tools get in the way of your work. Look for stable tools,
  with stable APIs and regular releases.
- A: Consider starting within an existing project and then spinning off. Do
  not be afraid to ask for help.
