# Science Lab : Using the Scientific Method to Create Amazing Web Experiences

https://events.drupal.org/seattle2019/sessions/science-lab-using-scientific-method-create-amazing-web-experiences

- Amy Shropshire (@AmyShropshire)

Make web sites better and communicate that quality to the client.

Client:

> I want to know that if I spend `X` in time and `Y` in dollars, then I will
> reach `n` customers and expect to get `Z` in contracts.

How can we reshape expectations, instead of saying "I don't know" or "it
depends"?

Failure is a valid outcome.

In what context is failure allowed? Not in business nor education, but in
science.

- Observe
- Hypothesis
- Test and collect data
- Analyze results
- Accept or reject the hypothesis
- Go back to the top
