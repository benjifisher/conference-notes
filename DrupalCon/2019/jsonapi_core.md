# Why will JSON:API Go Into Core?

https://events.drupal.org/seattle2019/sessions/why-will-json-api-go-core

Mateu Bosch (Lullabot)
Wim Leers (Acquia)
Gabe Sullice (not present, Acquia, recently a spec maintainer for JSON:API)

Actually the tense is wrong.

Two parts of the answer:

- Because we could.
- It made sense to do it.

We want to make Drupal API-first, and getting JSON:API into core is the first
step.
