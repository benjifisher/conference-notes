# Conference Notes

This repository holds notes from conferences we have attended.

- BADCamp
    - [2018](./BADCamp/2018)
    - [2019](./BADCamp/2019)

- DrupalCamp NJ
    - [2020](./DrupalCampNJ/2020/)

- DrupalCon
    - [2019](./DrupalCon/2019/)

- MidCamp
    - [2019](./MidCamp/2019)

- Boston Drupal Meetup
    - [2016](./BostonMeetup/2016)
    - [2017](./BostonMeetup/2017)
    - [2018](./BostonMeetup/2018)
    - [2019](./BostonMeetup/2019)
    - [2020](./BostonMeetup/2020)

- NEDCamp
    - [2018](./NEDCamp/2018)

- NERD Summit
    - [2017](./NERDSummit/2017)
    - [2018](./NERDSummit/2018)
    - [2019](./NERDSummit/2019)
