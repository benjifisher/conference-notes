# Keep Living the Dream

How to Work Remotely and Foster a Happy, Balanced Life

- Anne Stefanyk (Anne@Kanopi)

> I would love to work from home: so relaxing, free schedule

If you are not careful, it can be lonely, stressful, lack of exercise

- 94% of service professionals put in 50+ hours per week.
- 40% of knowledge workers never get mode than 30 minutes straight of focused work.
- Stress is transferable.

Shocking state of work/life balance in 2019

We all want a happy, calm day at work.

## As an employee

- Pants are not optional.
- Structure time for lunch.
- Create a work space and set boundaries.
- Good work requires good wifi.
- Create ritual. Bookend your day.
- Get a dog.
- Stretch (yoga, including eye yoga)
- Cut screen time: read Mel Robbins
- Mornings are YOURS (at least 20 minutes)
    - 20 minutes exercise, 20 minutes learning, 20 minutes rest is a good formula
- So are the evenings. Consider no screens after 8 PM.
- Sleep heals
- Meditation is medication.

## Company and Leadership

- Culture is clutch. Define them, then operationalize them.
    - "Hey taco" app for Slack.
- Know your why
    - Make continuous improvement in product and processes.
- Lead with radical candor. Be direct without offending.
- Give real-time feedback. Also check in quarterly.
- Collaborative leadership
- [Know your team](https://knowyourteam.com/) (good idea and also a product)
- Encourage good work ethic. (Smart is the outcome of hard work.)
- Recognize great work.
- Celebrate wins.
- Throw tantrums. (There are limits: do not dump on coworkers, for example.)

It is time to be calm and happy at work.

## Q&A

- Q: How do we teach our team to give feedback?
- A: 360 reviews, encourage people to review you. Coaching.

- Q: How do you help your remote staff feel connected?
- A: Every other Friday, remote-friendly recap. Monthly all-hands meeting, an some are game time. Buddy system for new staff. Try to have an annual in-person meeting. Show and tell (over Zoom or whatever). Encourage social channels on Slack. Book club. Lots of little things. Be video ready at all times.

- Q: Is psychological safety important? Being non-judgmental?
- A: Yes, that sort of thing is toxic. If someone is being short or grumpy, ask what is wrong. Help if they need it, get rid of them if they persist.

- Q: Do you have epic failures in view conferences?
- A: Our employee handbook has grown over time. Avoid back-to-back meetings.
