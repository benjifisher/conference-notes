# XSS Protection with Content Security Policy

- Geoff Appleby (@gapple)

Content-Security-Policy HTTP header

With `default-src: none`, you get no CSS, no JS (not even inline). Probably not what you want. `default-src: self` is a better place to start.

You can specify default domains and also domains for CSS, JS, fonts.

There is a Drupal module for it. It is smart enough to scan `.libraries.yml` files and figure out which domains should be whitelisted.

- [Content-Security-Policy](https://www.drupal.org/project/csp)

You can enable logging, but that can be a heavy server load.

CKEditor limitation: you need to add `unsafe-inline` or else CKEditor will break.

Make sure you add JS through libraries instead of hard-coding it in the theme, so that the module can find it.

If you really need in-line js, then you can use `<script nonce="...">` and whitelist the nonce in the CSP.

What about aggregation? We trust JS files in modules. If we are paranoid, then they are already on a RO filesystem. The aggregated files are on a RW filesystem, so less trusted. Forget about `public://` files.

Perhaps set it up to serve CSS and JS from a subdomain.

CDN: may as well allow the whole web. You can specify a path (e.g. to jQuery) but you have to worry about insecure older versions.

## Script integrity

The `<script>` tag has an attribute to specify the sha hash of the file.

## Feature policy

`Feature-Policy` header. Limited support in current browsers.

## Reporting API

Browsers send helpful reports when something goes wrong. Are you watching for these? Add a `Report-To` header. TIAMFT (still in beta)
