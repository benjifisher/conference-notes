# Best Practices for Securing Your Site

- Aurora Morales (@mkduckhorn)

The web has gotten more slick in the last 20 years, but less secure: 32% increase in hacked sites. (?)

Hack types:

- gibberish
- Japanese keyword
- cloaked keyword

These are hijacking your site's reputation to get SEO for their products.

How to clean and protect your site:

- User: pay attention to malware warnings from your browser.
- Site owner: if your search results contain warnings, you are in trouble
- Recovery: bit.ly/nohacked (Google resources)
    - fix problem
    - request review through Google search console

## Security best practices

Do not use weak passwords

### Safer login (password)

- at least 8 characters
- ...
- updating passwords regularly is *not* a must

Use pass phrases.

Use two-step verification (2SV) a.k.a. 2FA or TFA. A security key is more secure than the other methods. No wifi nor batteries needed.

Do a security check-up. Google accounts offer an audit.

### Safer website

Outdated software is like an open shoelace.

Prevention is key. Enable automatic updates.

Back up regularly and automatically.

### Safer connection

Use HTTPS.

- encryption
- authentication
- data integrity

(The slide mixes up the explanation of encryption and authentication.)

Use LetsEncrypt: free certs.

### Search console

g.co/SearchConsole

Optimize and secure your site.

## Summary

Make security a priority. Adopt and share best practices.

## Q&A

- Q: Just 8 characters for a password?
- A: Yes, at a minimum
- follow-up: some sites do not allow more than 8

- Q: How about password managers?
- A: OK. Rememeber that they can and have been hacked. Google does not recommend a particular one

- Q: Drupal does not have automatic updates.
- A: Thanks for letting me know.

- Q: Backups are useless unless you test restoring from them.
- A: OK

- Q: Is it risky to give access to Google Analytics?
- A: Not very.
