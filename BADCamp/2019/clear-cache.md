# Did you clear the cache?

A Contextual Tour of Rendering Efficiently

- Stephen Lucero (@slucero)
- Rob Powell (@robpowell)

## Cache metadata

- max-age
- tags
- contexts

## Scamalytics dashboards

Our motto: scam 'em and spam 'em

`max-age` defaults to `Cache::PERMANENT`; set to 0 for no cache.

Example usage: request data from an infrequently updated API. There is no need to call the API every time, so cache the result for a few hours.

Cache tags: these give us granularity.

Example usage: generate a report of how many nodes of each type.

Cache contexts: store more than one version of the data.

Example usage: per-user reports, i.e., customized based on the currently logged-in user. Similarly, you might have role-based reports.

## How does it work?

Think of a Drupal page as a render tree. Render arrays have child arrays and so on.

Bubbling is the process of rendering an array and passing cache information up to the parent.

If our dashboard has 4 sections, and each has its own cache tags and contexts, then cacheability of the page overall depends on the combined cache metadata.

## Behind the curtain

Look at the database. There are a bunch of `cache_%` tables. They all have the same structure:

### Cache ID, `cid`

It is a string, composed of keys and contexts.

### Cache tags

A space-delimited sequence of "targets" or bulk identfiers, used to invalidate/delete cache entries.

## Gotchas

- Invalidation vs. deletion
- Expiration vs. deletion
- Unknown content connections: cache system does not understand entity references.
- Losing cache metadata in custom templates: make sure to render `content` in order to caculate and bubble the metadata.
- CDN and Purge
- CDN tag hashing
- Bulk invalidation with `node_list` and other tags

## Resources

- Renderviz module
- Views custom cache tags
- Handy cache tags
...

## Q&A

- Q: When you say "HTML", is it equally true for JSON?
- A: Yes. We have been looking at the render cache, but there are others. (???)

- Q: Can you change it based on the type of output?
- A: You will use the same methods ...
