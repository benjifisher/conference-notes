# Anonymous Personalization Without Leaving Drupal

- Michael Lander (@michaellander)

What sort of personaliation do we mean?

A: Take what we know (e.g., location) and customize the experience based on it. For example:
- with Umami we might want to highlight different recipes based on location. 
- customization based on Android vs. iOS

How does this work with cached pages on a CDN?
A: Design server-side, leverage client-side info. Looks like JS to me.

Introducing [Smart Content](https://www.drupal.org/project/smart_content) module.

N.B.: This is not about access control. It relies on data from the browser, which can spoof the data it sends.

Smart Content is the API module. Related modules use the API and let you control blocks and other stuff.

With the SC Block module, you get options in the block config. E.g., show/hide the block based on whether the browser is a mobile device.

E.g. Check for a cookie. Based on whether the cookie is set and (if so) what the value is, set the page banner.

Segments: lets you reuse a set of conditions.

## How does it work?

It includes some JS libraries. When the page loads, get data from the browser. Go through conditions and reactions. There is some optimization in managing the decision tree. The reactions basically replace placeholder tokens.

## What sorts of conditions can we check?

- location
- GDPR
- time of day
- cookie
- if they visit an obsolete link
- ...

## Live demo

This is being used on the Elevated Third web site.

## Roadmap

One issue away from beta. (Bug related to Layout Builder)

Add features, integrations.

2.x will be refactoring, cleanup. Maybe we overused the plugin system. Separate decisions from reactions. Decouple storage (block storage? config?). Improve UX.

## Q&A

- Q: Can you read browser history?
- A: I hope not. There would be privacy concerns.

- Q: Does it work with CDN?
- A: yes

- Q: What about analytics?
- A: use data layer module

- Q: On Elevated Third site, were you swapping out a view?
- A: Yes. We are passing args to a view. Prefiltered, choosing a block.

- Q: Can I detect customer vs. staff? How can user support mimic what the customer sees?
- A: Do not change too much. The 2.x version will provide better masquerading.

- Q: Can you expose a block with debug info?
- A: You could do it with JS.

- Q: For the Elevated Third page, what conditions do you use?
- A: Demand Base, an external service that is based on IP address.

- Q: Are all the shown/hidden blocks and block variations in the HTML markup, cached on the CDN, and then selectively shown based on client-side JS?
- A: No, it is doing separate HTTP requests to fill in the placeholders.

- Q: Yesterday we saw if-then-else module. Any integration?
- A: Not yet. Maybe we can use the same JS library for the UI.
