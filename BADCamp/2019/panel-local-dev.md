# Local Development Environments Panel Discussion

- Tess Flynn (flightdeck) could not make it.
- Randy Fay (@rfay): DDev
- David Platek: Acquia
- Sean Dietrich (sp?): Docker
- Mike Pirog: Lando

## DDev

https://ddev.readthedocs.io

- no GUI
- quick to get started

## Acquia Developer Environment

- Uses Lando or DrupalVM
- integrates with services esp. Acquia Cloud
- works with BLT
- Comes with Lightning
- no GUI

## Docksal

- lightweight wrapper for Docker/Docker Compose
- lots of integrations
- customizable
- no GUI
- needs `bash`
- first pull is slow
- no hooks
- flexible: multiple config files

## Lando

- cross platform, one-click installer
- builds on Docker
- one config file in your repo defines the local env
- consistent environment 
- infrastructure and tooling are both configured, versions specified
- integrations with hosting providers, CI/CD, ...
- no GUI
- file sharing is still slow
- free and OS

## Questions

How easy is it to use on different operating systems?

- DDev: anywhere Docker will run
- Docksal: on Windows, need bash installed; on Windows Home, run VirtualBox version

What exactly do these things do?

Set up a local dev environment.

- consistent for all members of the team
- critical when you have to switch between projects

How hard is it to get Gastby (nodejs) and Drupal (apache, mysql) running together?

- Docksal: yes, just configure your Docker compose file.
- DDev: yes, ditto; maybe run everything on the same container
- Acquia: yes, since it is built on Lando

Is there a concern about the future of Docker?

We are not worried (yet).

The common tool seems to be Docker. How did you arrive at that?

- Docksal: started with VMs, decided that containers scale better
- Lando: pretty much the same. Each project needs to manage its own dependencies.
- DDev: maybe we will see someone do it with Kubernetes instead of Docker compose. Maybe Flight Deck already does.
- All of these hide Docker until you need it.

Can it run on production?

- Docksal: we have an environment variable that is supposed to be set differently on dev/qa/prod.
- DDev is agnostic about the server environment. It is careful not to let anything leak out. DDev containers are not intended for production use.
- Lando: it is not recommended, but people are doing it.
