# You are not a candle; don't burn out

- Michael Hess (mlhess)

## Why am I giving this session?

Assumptions:

- color
- reading
- counting
- know your ABCs

Play two games:

- fill in 3 columns (number, letter, Roman numeral)
- read color names, color of letters

We are worse at multitasking than we think. It does not save time.

You can measure it yourself: wear a FitBit and compare pulse when stressed and multitasking vs. stressed but not multitasking.

Think about the number of tasks you do

- per hour
- per day
- per week
- per month

Computers are really good at context switching. People are bad at it.

Self-interruption takes 10-15 minutes to get back in the swing. External interruptions take up to an hour.

Make a table of # of projects vs. how much time is lost to context switching. E.g. 2 projects 40-40-20; 3 projects 20-20-20-40.

Multitasking leads to stress, anxiety, and burnout.

This is not good for health and safety, nor for quality of work. This has hidden costs ... pretty steep costs.

Technical debt (TD): short-term gain, long-term expense. Causes:

- lack of standards
- causes future unplanned work
- deadlines set without enough information
- too much complexity
- small things not loosely connected
- lack of testing

High WIP leads to high technical debt

If simple things take a long time to do, then there is probably a lot of technical debt.

TD is not always bad. Recognize it and make it visible. Track it.

TD _is_ bad when it is caused by multitasking.

High TD and high WIP lead to burnout.

High WIP leads to lots of rework.

- bugs
- defects
- human errors

## What to do

- monitor e-mail, slack, etc. usage
- understand the business value of all projects
- have meetings in chat
- Keep a kanban board and enforce limits
- leave your job
- check your e-mail at defined times

## Take aways

- Keep WIP low
- Monitor TD
- Prioritize your projects
- Keep a solid work/life balance. Your job is not worth it.

## Q&A

- Q: All roles are routers or processors. Does the advice apply to both? What id I am a router and my failure to reply blocks someone else?
- A: Get someone to help. Make sure each of your team members have at least two items in their backlog. Keep tasks small and well defined.
