# WHY FORK DRUPAL? THE PHILOSOPHY BEHIND BACKDROP CMS

[backdrop](https://backdropcms.org/)

Is there room for another CMS?

Before there was WP and SquareSpace, using Drupal for every site was not such
a bad idea. (Maybe 10 years ago.)

We hope that Backdrop will fill a niche.

Jen and Nate are the founders of Backdrop.

Before starting Backdrop, they

- built a lot of sites
- did a lot of training
- contributed to Drupal core
- worked in the Drupal community (BADCamp, DrupalCOn SF 2010)

Backdrop is designed to serve the existing D7 user base, not the "ambitious
digital experience" market that Drupal has targeted.

They established a Project Management Committee (PMC), a model used by Apache
projects. The members represent the different stakeholders, and they also aim
for diversity in race, gender, geography, ...

The PMC is responsible for

- conflict resolution
- general direction of the project
- adhering to and, if needed, modifying the project's philosophy

In contrast, Drupal (until recently) had very little in the way of stated
philosophy, and ignored it when making decisions.

## [Backdrop Principles](https://backdropcms.org/philosophy)

1. Easier updates: BC is important.
2. Simplicity: Write code that the majority can understand.
3. Focus: Only include features that benefit the majority.
4. Extensibility: Ensure that Backdrop can be customized.
5. Performance: Meet low system requirements.
6. Release on time: Plan and schedule releases.
7. Freedom: Remain both Free and Open Source.

Mission statement:

> Backdrop CMS enables people to build highly customized websites affordably, through collaboration and open source software.

## How can we make web development more affordable?

1. Increase OOTB functionality.
2. Improve the UX.
3. Improve the DX.
4. Decrease server resource usage.
5. Make maintenance faster and easier (or automatic).
6. More!

## Release schedule

- Jan 15
- May 15
- Sep 15

## Core and contrib modules

- Many of the most popular Drupal modules are available.
- 501 total projects (modules, themes, layouts) on GitHub
- 400 with full releases, on the BD web site.

## Security

- Nate is on the Drupal security team.
- Backdrop has its own security team.
- We coordinate releases with Drupal.
- There is a private security issue queue.
- Security releases are made on Wednesdays.
- The BD security team has commit access to all contrib projects.
- There is a process for transferring ownership of unmaintained projects.

## Community

- 10K downloads
- 1.6K members on backdropcms.org
- 653 live sites reporting
- 86 core contributors
- 77 contrib group members

## Contact us

...

## Events

- Drupal camps
- other OS events
- Parties (Jan 15 2019 b'day party)
- Local meetups (starting Jan 2019) in LA, SF (combined Drupal-Backdrop)

## Q&A

- How does the security policy follow from the philosophy? Do you need another
  point in the philosophy?
  A: it falls under easier updates: if an insecure module is not fixed, then
  you cannot update it.
