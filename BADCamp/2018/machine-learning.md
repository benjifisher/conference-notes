# USING MACHINE LEARNING TO AUGMENT YOUR CONTENT

- Angelo Porretta (captainpants)

## Summary

- What is it?
- What tools exist?
- How do we integrate with Drupal?

## What is machine learning and natural language processing?

- ML: statistics & data driven
    - languages evolve
- NLP: just what it sounds like
    - sentiment analysis (generally represented by a real number from -1 to +1)
    - entity analysis: sort of like tokenization; names, dates, ...
    - topic segmentation
    - language identification

## What 3rd party tools exist for NLP?

## Datamuse

Free, open (at least for experimentation).

Good tool for finding relationships between words:

- synonyms
- antonyms
- meronyms (trunk > tree)
- rhyming

## Textrazor

Commercial but powerful

- topic segmentation (good)
- entity analysis
- not so good at sentiment analysis

## Amazon comprehend

Commercial but powerful

- pay for what you use
- cheap
- good entity analysis
- decent sentiment analysis

## Google Natural Language API

Commercial but powerful

- pay for what you use
- cheap
- does most tasks well

## Practical uses

Entity analysis:

- Look at Grammarly, Hemingway editor

Sentiment analysis:

- customer reviews
- social media analytics

How accurate is it? It has trouble with sarcasm.

Topic segmentation:

This is very relevant for Drupal with its taxonomy syste.

[Google Natural Language Autotag](https://www.drupal.org/project/google_nl_autotag):
Applies terms automatically based on text.

## Additional resources

- intro to deep learning - sentiment analysis
- building an intelligent media monitoring chatbot for Slack
- monitoring media reaction to Facebook earnings call

## Q&A

Q: How can we reduce false positives?
A: That is up to the provider.

Q: Could there be human intervention built in to check the machine's results?
A: We could review low-confidence results.

Q: Where is this headed for CMSs?
A: There are already two contrib modules. Working on auto-suggest for news
based on sentiment scores.

Q: You can also analyze photos. There are services that can identify
pornography.
A: Yes, we can use other services for filtering visual content.

Q: Is sentiment analysis useful for spam prevention?
A: Only if you are biased (all negative reviews are spam).

Q: When topic segmentation generates taxonomy, is it based on a standard
hierarchy?
A: Yes, at least for Google's service.
Q: (follow up): will the service get feedback when you correct its mistakes?
A: ...

Q: Can we automate schema.org metadata?
A: It can pull out dates, but you should be careful ...

Q: From what I have seen, NLP is anglo-centric. What about other languages?
A: I do not know. NLP can identify the language, but other functions ... ?

Q: Can it distinguish between American and British English?
A: Potentially.
