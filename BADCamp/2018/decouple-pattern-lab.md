# DECOUPLE PATTERN LAB FROM YOUR D8 THEME (SF.GOV)

Slides at http://slides.com/isramv/decouple-your-pattern-library-from-your-theme#/

## Pattern library

- can be used on other projects
- requires Twig

Usually the PL lives inside the Drupal theme.

- Particle
- Emulsify
- Bear Skin

## Requirements for this project

- https://designsystem.digital.gov/ (USWDS, starting point)
- need it fast
- independent of Drupal
- don't care which system we use

## Ten steps to decouple

1. Choose the breed (e.g., PHP Twig standard)
2. Add watchers to do the compiling (gulp, grunt, webpack)
3. Generate a single CSS and single JS file.
    1. Edit PL head file to include them.
4. Create a Drupal theme.
5. Enable the component libraries module.
6. Map your PL to Drupal. The library does not have to be inside the theme.
   (Use `../../..` or whatever.)
7. Use the single CSS and JS via libraries (in the theme).
8. Use composer to require the external PL.
    - `composer require oomphinc/composer-installers-extender`
    - Edit `composer.json` to say where Drupal libraries should go.
    - `composer require ...`
9. Use the PL templates with Drupal.
10. Profit

## Q&A

- Decoupling is good. Pega goes one step further, decoupling the Pattern
  Library from Pattern Lab.
- Beware of the Drupal version of Pattern Lab. It will couple instead of
  decouple.
- What about the `ui_patterns` module?
