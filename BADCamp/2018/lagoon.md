# GOVCMS ON LAGOON, THE AUSTRALIAN GOVERNMENT MOVE TO A 100% OPEN SOURCE PLATFORM

- [GovCMS home page](https://www.govcms.gov.au/)
- [GovCMS Drupal distribution](https://www.drupal.org/project/govcms)
- [GovCMS organization on GitHub](https://github.com/govCMS/)

## Lagoon is built on containers:

- CLI tools
- MySQL
- HTTP
- ...

## Openshift/Kubernetes

## GovCMS version 1.0 vs 2.0

- More automation, a lot faster
- all OSS
- Saas and PaaS on same platform
- more secure

## Stack:

- Drupal
- Lagoon
- OpenShift
- Kubernetes
- AWS (3 availability sones, auto-scaling)

## CI and testing

- Behat
- PHPUnit
- Managed upstream
- Extendable
- Scaffold validation
- [Drutiny](https://drutiny.readthedocs.io/en/latest/README/) (auditing)
- Dedicated test image

## Admin UI

contributing back

## Migration

- low risk
- [AWX](https://www.ansible.com/products/awx-project) Ansible (UI for Ansible)
- one-click migrations
- idempotent
- codebase, database, files, Solr
- removes redundant modules, installs new ones
- creates new repo and deploys

## Q&A

- With most of the code in containers, what lives in the repo?
  A: In SaaS, just a custom theme. In PaaS model, could include additional
    contrib modules and custom ones

- How can I try out Lagoon?
  A: We have a booth at BADCamp.

- Can different PHP containers share opcode cache?
  A: No, but with PHP 7.1 it does not matter much.

- Have other governments (outside Australia) adopted GovCMS?
  A: Not that we know of.

- How much does the public roadmap affect amazee.io?
