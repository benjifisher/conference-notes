# ADVANCED WEBFORMS

> Jacob Rockowitz

- leveraging hooks
- 

## Testing

> Tests confirm expectations.

Webform tests OOTB confirm

- rendering
- processing
- validation
- settings
- access

## Webform entities

> Everything in D8 is either a plugin or an entity

(Not entirely true, but close ... and maybe it should be true.)

They keep track of source entity, so you can use the same webform (in a block)
attached to many nodes.

## My take

This presentation went about 1.5 to 2.0 times too fast. Jacob was scrolling
through code and through tables and I could not keep up.
