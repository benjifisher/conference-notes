# BADCamp 2018

Benji attended BADCamp for the first time this year.

- [Advanced Webforms](advanced-webforms.md)
- [Best Practices: How we Run Decoupled Websites With 110 Million Hits Per Month](decoupled-high-traffic.md)
- [Decouple Pattern Lab From Your D8 Theme (sf.gov)](decouple-pattern-lab.md)
- [Diversity & Inclusion: Building a Stronger Drupal Community](stronger-community.md)
- [Drupal 8 Case Study – Stanford Cantor Arts Center Redesign](cantor-arts.md)
- [Feeds UI + Migrate Engine = Dream Migrations and Imports](feeds-migrate.md)
- [Gatsby and the JAMSTACK: An Introduction for the Drupal/LAMP Minded](gatsby-jamstack.md)
- [Gitlab Has Come to drupal.org](gitlab-drupal-org.md)
- [GovCMS on Lagoon, The Australian Government Move to a 100% Open Source Platform](lagoon.md)
- [Making Accessibility Audits Easy: Tips and Tricks for Auditing](accessibility-audits.md)
- [Protect This Drupal House: How to Run and Respond to a Security Audit](security-audit.md)
- [Using Machine Learning to Augment Your Content](machine-learning.md)
- [Why Fork Drupal? The Philosophy Behind Backdrop CMS](why-backdrop.md)
