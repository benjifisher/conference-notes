# Making Accessibility Audits Easy: Tips and Tricks For Auditing

- Michaela Blackham (FE dev at Aten)

Even without an audit, let's try to make fewer mistakes.

Interaction, organization, ease of use have always been important to Michaela.
Also, she had a cousin with Duchens' (sp?) disease.

53 M adults live with some form of disability (CDC).

## What is accessibility?

Easy to obtain, use, and understand (POUR):

- Perceivable
- Operable
- Understandable
- Robust

## Who needs an accessibile web site?

Everyone!

There are different sorts of impairments:

- Visual
- Motor/Physical
- Auditory
- Cognitive

## Accessibility Audits

Guidelines:

- WCAG 2.1 (released June 2018)
    - A, AA, AAA levels of compliance (most people aim for AA)
- 508C
    - Mostly applies to some regulated organizations

Creating an accessible site is more than just a checklist.
Do you want to pass, or do you want visitors to like your site and come back?

E.g., location of labels on input elements is not going to be caught by
standard requirements.

- Client facing
- Internal (during development).
- Personal

## What does an audit entail?

- requirements (WCAG or 508C)
- pass goal: compliance or enjoyment?
- testing
- desktop or mobile

## Testing and tools

### Automated

You need to know how to interpret the results, and how to fix problems.

- [Wave](https://wave.webaim.org/)
    - well known
    - web site or Chrome extension
    - false positives on color contrast
- Lighthouse
    - integrated with Chrome Canary
    - Does SEO, performance, accessibility, and best practices
    - creates a report for client
    - gives your site an overall score

### Manual

... because the automated tests are not perfect

- keyboard
    - pass/fail: can you navigate the site or not?
    - actionable items
    - focus styling
    - logical order
- screen reader
    - JAWS (widely used, expensive, M$ only)
    - NVDA (free, M$ only)
    - Voice Over (VO) (Mac only, built into the OS)

Screen reader testing:

- entire experience
- actionable items
- navigation
- content
- follow your goal (user stories, testing scenarios)

## Tips and Tricks

Project Managers:

- Get access to GA
- Include who needs to make the edits

Content Editors:

- DO NOT WRITE IN ALL CAPS
- Check hierarchy with the "no styles" option in Wave
- Outline extension in Chrome
- Use a screen reader
- Read the site to someone over the phone

Develpers:

- Color contrast checker in Chrome
- Is your site responsive?
- Manually check contrast errors
- Test over images: use a background in case the image does not load
- Zoom

Keyboard:

- Test the "skip to main content" link. Where is the focus, next tab?
- Default focus styling
- Interact with actionable items
- Test with the ChromeLens extension
- Test forward and backward (e.g., Shift-Tab)

Screen reader:

- Set preferences
- There are VO tutorials on the Mac
- Turn your screen off and throw away the mouse
- Test a site you are not familiar with
- Play all
- Dates (maybe not what you expect)
- Short words (1-3 letters) are sometimes read letter by letter
- Vague link text (you can add an ARIA label)
- Get comfortable, repeat the tutorials
- Sometimes the screen readers can be the problem

## Demo
