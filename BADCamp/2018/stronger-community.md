# DIVERSITY & INCLUSION: BUILDING A STRONGER DRUPAL COMMUNITY

- Tara King (sparklingrobots)
- Ruby Sinreich (sp?) (rubyji)

## Definitions

> Diversity is being invited to the party. Inclusion is being asked to dance.

## Advantages of diversity

### More creative and collaborative

... also, things can take longer

### More effective and successful
