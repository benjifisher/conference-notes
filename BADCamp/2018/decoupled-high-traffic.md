# BEST PRACTICES: HOW WE RUN DECOUPLED WEBSITES WITH 110 MILLION HITS PER MONTH

- Michael Schmid (@Schnitzel)

Please ask questions. Do not wait for the end.

## The projects

### Ringier Axel Springer (media company with several web sites)

- started late 2016
- multiple front ends, single backend (WOPE)
- beobachter.ch (consumer watchdog)
- bilanz.ch, ... (Bloomberg of Switzerland)
- ...

There is a single Drupal backend and several front ends for different sites.

### Swiss Paraplegic Center

- 2018
- single front end
- Drupal beackend

## Summary

- Infrastructure
- Platform
- Code
- People
- Learnings


## Infrastructure

- visitor
- AWS load balancer: sends to one of 3 availability zones
- OpenShift routers (SSL termination, load balancing)
- Kubernetes master (databases?)
- compute nodes (hold the containers) ... auto-scaling
- gluster mount (DB files, Solr inexes, ...)
- Amazon EFS (user files)
- BURP (backup) - in just one availability zone

## Platform

- Completely Dockerized
- Lagoon, Kubernetes, OpenShift
- Fully auto scaled

What is in each compute node:

- visitor
- AWS load balancer
- OpenShift routers
- Varnish service
- Nginx/PHP paired containers (auto scaled)
- MariaDB, Redis, Solr (one copy each per availability zone)
- Separate container for CLI services (via Lagoon) alongside compute nodes

Auto scaling is based on CPU usage, but there are other measures that would be
better.

Isolate risky things like drush in the CLI container, where the HTTP
containers cannot get at it.

For HA, double the Varnish, Redis, and Solr containers. Use a 3-node Galera
cluster for each DB.

For decoupled Drupal, add a Nodejs service.

For multiple front ends, replicate the Nodejs service (and its Varnish
containers).

## Code

(battery runs out)

## People

## Learnings
