# DRUPAL 8 CASE STUDY – STANFORD CANTOR ARTS CENTER REDESIGN

## Summary

- Why redesign?
- Technology evaluation
- IA and sitebuilding
- Stanford Events importer
- Takeaways
- Q&A

## Previous site

Previous site 2006: no CSS, no responsive design ...

Project cocerns:

- Cantor leadership
- Previous agency

Redesign concerns:

- bold, photo-centric design
- flexible, component-driven content strategy
- import events from feed
- use modern FE technologies
- Be careful about using images (no cropping, no overlays)

## Technology evaluation

What technology should we use? (Consider WP, but there were reasons to go with
D8.)

Content architecture?

Base theme?

Theme components?

Integration (events feed)

Evaluate
- current state of D8 (core, contrib)
    - core stable, but layouts and components still using contrib
    - Feeds for D8 seems dead in the water
- SWS tech (Stanford Earth project) - concurrent project elsewhere in the
  univ.
    - Stanford Earth was the first site on Stanford's D8 platform
    - It had some good content components, but not much could be reused. :-(
- Compare to needs and timing of the Cantor project
    - Reuse when it makes sense.
    - Follow best practices.
    - KISS
    - The Stanford dev who would maintain it was familiar with Booststrap.

## IA and sitebuilding

Flexible content architecture:
- UI Patterns module
- Paragraphs and ER
    - nested paragraphs for layout elements
    - IEF for better editorial experience
- View modes, Paragraphs, and UI Patterns make it so that content editors can
  reuse *and override* layout through the UI.

No cropping:

- As usual, use `<picture>` elements and source sets.
- some polyfill to help with sizing.

## Stanford Event Calendar Feed

- XML feed
- Can filter by organization
- Title, description, date, ...
- Includes one-time and repeating events

How they did it:

- ECK for custom entity type
- Migrate API and Migrate Plus
- Ultimate cron (1 AM daily)
- Custom D8 module

## Takeaways

- Image handling respected client requirements
- The team was flexible enough to manage changing requirements.
- Site editors were happy.
- Event importer was better than copy & paste.
- Pantheon hosting provided good dev workflow.
- Hook 42 and Stanford Arts are still friends.

## Q&A


