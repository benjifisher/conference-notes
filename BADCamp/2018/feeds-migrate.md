# FEEDS UI + MIGRATE ENGINE = DREAM MIGRATIONS AND IMPORTS

- Irina Zaks, [Fibonacci Web Studio](http://fibonacciwebstudio.com/)

## Summary

- Content migration tools for D8 in 2018
- Compare Feeds and Migrate modules
- Feeds-Migrate (in development)

Your web site supports the mission and operation of your organization.

Moving content into your site


- structured data
- sources (other sites, documents, databases)
- one time/periodic
- update, replace, or preserve existing content
- monitor import state

## Methods

- one-click upgrade (D6 or D7 to D8 new site)
- Set up migrations via drush and YAML files
- Feeds module
- Custom scripts (bad idea)

## Demo

Demo of alpha3 version of Feeds, still based on D7 Feeds module.

## Q&A

Q: In new Feeds module, can you mix and match YAML and UI for creating
migrations?
A: maybe, if someone implements it. There may be parts of custom migrations
that the UI module cannot handle.

Q: Will config be stored in YAML? Can you run via drush?
A: Probably.

Q: Have you created issues that will lead to alpha1 version?
A: Most of the issues have been created. There is a roadmap, not yet
    published.

Q: Did you start with the UI?
A: No, we started with the Migrate API already in core.

Q: Feeds for D7 is still in beta. What are the chances for a full release?

Q: Will it handle Migration lookup?
