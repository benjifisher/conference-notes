# PROTECT THIS DRUPAL HOUSE: HOW TO RUN AND RESPOND TO A SECURITY AUDIT

- Alice Freda, Kalamuna

Remember the Loma Prieta earthquake in October, 1989?
Alice was a kid then, and developed a habit of being prepared for disasters.

Blog post in the works, based on this talk, at blog.kalamuna.com.

## Intro to security audits

### Why bother?

- Trust
- Protect user data
- Look at what happened to Equifax's share price after they were hacked.
- SEO (can get blacklisted by search engines)
- Prevention (since hacked sites can be hijacked to attack others)

## Common attacks

- Clickjacking
- XSS (Cross-site scripting): malicious scripts are injected into vulnerable
  sites.
- SQL injection

## Prevent unauthorized content from being added to your site

- Code review

### Where to start

- Comments (do we need them?)
    - Avoid anonymous commenting, and restrict permissions if you allow it
    - CAPTCHA or other tools
- Forms
    - Use Drupal's recommended practices, Honeypot module
- Custom code
    - Use database API to avoid sending unsanitized code to the database
- WYSIWYG config
    - Limit text formats
    - PHP filter should be turned off
    - Limit uploads by filetype
    - Sanitize SVG files

## Helpful modules

- Hacked!
- Security Review
- Paranoia
- Honeypot
- Security Kit (content security policy, anti-clickjacking settings)

## Check your site's status report and dblog

## Limit exposed information (e.g., error messages)

## Restrict Drupal access

- Pay attention to admin users; disable stale accounts with extra permissions
- Check for password policy (and there is a module for that)
- Review sign-up process (e.g., require e-mail verification)
- Automatic logout
- Create user roles and give them only the permissions you need.
- Restrict access to the login form
    - Change the default path for the form.
    - Limit by IP
    - TFA
    - CAPTCHA
    - VPN
- Limit access outside of Drupal
    - Hosting control panels
    - Files (FTP, SSH)
    - ...
- Look at files on the server
    - Proper permissions on the server
    - Check files other than the Drupal installation
    - Are there default Drupal files on the server?
- Secure DB
    - Prefixes (security through obscurity) and DB name
    - Appropriately complex DB passwords.

## Talk to your admin users

- security awareness
- how they store and share passwords
- awareness of security on their own computers

## Check for updates, then run them

- Know the release schedule
- Follow notifications via e-mail, Twitter, RSS, ...
- Evercurrent module

## Patches

You may need to apply a patch if you are using a distribution that is not
updated promptly after a security update to core.

## Remove unnecessary modules

## Preventive care

- Automated backups
- Malware scanning
    - ClamAV for scanning, and `clamav` Drupal module
- Firewalls
- HTTPS/HSTS

## Where to learn more

- see the slides or blog post, when available

## Q&A

- How do you change default login page?
  A: it needs custom code
- There is a dedicated content-security module
- Treat upgrades from pre-release versions to full releases as security
  upgrades
