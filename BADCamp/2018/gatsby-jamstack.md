# GATSBY AND THE JAMSTACK: AN INTRODUCTION FOR THE DRUPAL/LAMP MINDED

- Nick Lewis, ZivTech

You can develop a hatred of JS frameworks if you have to work with sites that
are someone's first Drupal project and someone's first React project.

Why we fell in love with Gatsby, and React components.

## WHat is Gatsby?

- a React app
- opinionated, esp. about perforance
- good start if you think React OOTB is too thin
- The project lead considers it a "website compiler".

## JAM Stack

- JS
- APIs
- Markup

Secret sauce:

- prod pages are flat files
- interaction is client-side
- there is a back-end CMS

Practical advantages

- cheap (hosting)
- fast

## Workflow

- Drupal
- Gatsby
- JAMStack
- CDN

## [Code](https://github.com/zivtech/gatsby-drupal-kit) (how to get started)

(Based on
https://github.com/gatsbyjs/gatsby/tree/master/examples/using-drupal)

- Drupal: install JSON API module
- Set up an account on Netlify (pulls pages from the Drupal site?)
- Gatsby uses GraphQL queries to consume the JSON. This is the hard part.

### Demo sites

- Drupal: https://dev-drupal-gatsby.pantheonsite.io/
- Gatsby: https://clever-gates-db60f2.netlify.com/

## Q&A

- What are your pain points?
  A: Inheriting a site based on Gatsby and Contentful.
- Basically, you decide which pages (nodes, terms, views) you want to pull
  into Gatsby, and then write a GraphQL query to do it?
  A: right
