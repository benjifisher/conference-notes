# KEYNOTE: How can we prevent the Orwellian 1984 digital world?

- Micky Metts

Economic liberation vs. ...

- convenience
- corporate perssure/coersion

How I was introduced to my own personal power

Google services are free (supported by advertising), convenient, bundled ...
are there truly free alternatives?

People w/o personal power typically wonder whether they fit in.

One problem with Google as SSO to all of our web accounts is that it is also a
SPoF. Another problem is that the advertisements follow us everywhere.

## Building blocks of freedom

- Free Software
- Cooperative Platforms
- Solidarity Economy
- Personal Power
