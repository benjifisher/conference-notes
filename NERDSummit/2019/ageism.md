# Ageism – Let's expose this one too! The "future You" will thank you

- Sharon Rieger

## What is ageism?

stereotyping and discrimination based on age

local and global impact: when one disempowered group gains recognition and
footing, it can help amplify and empower other such groups.

Generational diversity matters, too.

Older consumers are a growing market for technology.

## Why talk about it today?

Age discrimination in the workforce sends many seniors into poverty.

When an older person loses a job, (s)he

- spends a longer time unemployed
- take a pay cut if they do find a job

Ashton Applewhite:

> No one is born prejudiced, but attitudes about age as well as race and
> gender start to form in early childhood.

Blame the media.

## Next steps

Give credit. Show respect. 

Young activists turn age discrimination into a movement.

Some well-intentioned but ageist comments:

- You look good for ...
- Young lady or young man
- Are you still ... ?
- Back in your day
- My grandma is so adorable.
- 60 is the new 30
- Old dogs can't learn new tricks.
- Millenials are ruining ___

