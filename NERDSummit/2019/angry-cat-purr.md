# Getting an angry wet cat to purr

... turning an unhealthy client relationship into a productive one

- Donna Bungard

What do you do when things ... do not go according to plan?

## Overview

Sometimes everything goes according to plan. Stakeholders understand and are
happy.

Today we are talking about the other time. Stakeholders are not happy.

Why? There are no perfect projects, just great teams working together.

## Herding Cats

- Establish roles
- Communication methods
- Define success
    - MVP
    - Long-term goals
- Flexible process

Be aware of schedules: time zone, travel, schedule

Ask the right person the right questions

Use the appropriate method: e-mail, text, slack, voice

Get to know the people you work with.

What are OUR long-term goals?

MVP: What do *we* need RIGHT MEOW?

Process is king ... a flexible king.

## Empathy for a hiss

A hiss can be a good thing. (Better out than in.)

Listen to negative feedback. Ask questions and understand the problem.

- When did the problem start?
- Where is the data coming from?
- What would success look like?

What it can tell you:

- lack of understanding: goals or processes
- issues, scope, priorities may have changed or not been communicated
- they may need help showing value to the stakeholders

There is a hiss; two options:

- Defend your team
    - defensiveness
    - mistrust
- Explore the question, ask for help

Sometimes bad news is the best way to learn something.

## Playing well together

Client stakeholders

Agency stakeholders

We all have to work as a team.

- Be transparent
- Ask questions
- Know the people, not just the roles

What if the stakeholders are *not* aligned?

- Find time for a call.
- Get it on the table.
- Discuss the needs.

## When they are on the ceiling

It is not productive, but it can happen.

What is causing it?

- process or progress were not communicated well
- client's corporate culture breeds stress
- launch date vs. KPIs

What if it is a big issue, and your team made a mistake?

Own it.

- Agree on the problem.
- What aspects of the issue are most valuable?
- What is needed to address it?
- Propose a timeline.

What if you delivered exactly what they requested, but the client is still not
happy?

- Find common ground.
- Determine what resources are needed to fix it.
- Prioritize other work along with the new work.

## Grooming: just say no to hairballs

Do not ...

- blame nor be defective
- forget to showcase the value of what we did deliver
- miss a chance to speak positively about future opportunities

Do ...

- define next steps
- be forward thinking
- focus on the combined team

## Q&A

- Q: It is hard to find where something went wrong without placing blame.
- A: Use a retrospective board. Tell them that you need their help to figure
  out what went wrong.

- Q: At what point do you consider firing the client?
- A: When demands for resources outweigh the value

- Q: How and when do you communicate with your own team for things like coming
  up with a timeline?
- A: Have a dev on all calls (except the ones that talk about dogs). Never
  give a timeline without input from the dev team.
