# Modernize any Codebase through Tooling and Technique

- Michael Lynch

Slides: https://mtlynch.page.link/rTCx

## Modern Software Techniques

- automated testing
- code coverage
- +4

## Automates testing

### Unit testing

- most granular

### End to End testing

- least granular
- test a complete scenario/use case/workflow

## Code coverage

- What fraction of LoC are exercised in automated tests?

## Static Analysis

- pyflakes (for Python)
- CodeSniffer (PHP)

- usually quicker
- e.g., find unused variables, which may be bugs

## Linters / formatters

## Continuous Integration (CI)

- practice: verify that new code works with existing codebase
- automation: tools to create clean environments to test this

## Containerization

- Docker

## Where to start?

1. Containerization
2. Continuous Integration
3. End to End testing
4. In any order
    - Linters / formatters
    - Unit testing
    - Static Analysis
5. Code coverage

After 1-3, you can start modifying code with confidence that you are not
breaking anything.

## No new taxes ... on developers

Find a way to make it happen automatically during the existing workflow.

## Case Study in Legacy Code

NYT library for extracting searchable, consistent ingredients from
natural-language recipes.

- https://github.com/NYTimes/ingredient-phrase-tagger

### Docker (Containerization)

- capture dependencies in code
- useful for systems that are already working

### Stabilization (Continuous Integration)

- Get a Travis account (free for open projects).
- Enable your repository.
- Add `.travis.yml`.

### End to End testing

At this point we have not necessarily looked at the code, but that is OK.

### Rehabilitation

`pyflakes` identifies some unused variables, but maybe they are part of some
magic. At this point, we can delete them and run the end-to-end test to see
whether they really matter.

`yapf`: Yet Another Python Formatter

- supports `--diff`

### Our first unit test

...

## Final thoughts

- Add new tools without disrupting workflow
- Work incrementally
