# Preventing Burnout

- Michael Miles @mikemiles86

Dev(up): developingup.com

Slides: https://bit.ly/NERDBurn

## What is burnout; why is it bad?

1. Feeling of Dread
2. Exhausted
3. Easily annoyed
4. Dazed

Result: you become DEAD weight.

Another result: domino effect

## Types of burnout

### Worn out: too much to do, too little time

#### Cause: disrespect

- no recognition
- no control

#### Treatment

- encouragement
- (self) find a plan to get out from under

#### Prevention

- Teamwork

### Frenetic: more and more and more and ...

#### Cause: mistrust

imposter syndrome ... do not trust self ... take on too much work

manage the team ... do not trust others to do it right ... do it yourself

#### Treatment

- Develop a plan

#### Prevention

- Learn to say no. Find a way to get it done with the team if you cannot do it
  yourself.

### Under challenged: so bo-o-o-o-o-o-red

#### Cause: Emptiness

#### Treatment

- Take control
- Ask for more work

#### Prevention

- Set goals

## Burnout in general

Cause: stress
Treatment: reboot
Prevention: ask for help

Burnout is time-critical. Do not let it fester.

## Q&A

- Q: What about personal stress?
- A: The effects are similar. Some of the treatments still apply. A good
  boss/manager will help you find a plan for dealing with it in or out of
  work.

- Q: What about gratitude? Is it encouragement or a power play?
- A: It depends. If you show gratitude to everyone the same way, it is
  probably a power play. If you know how people *want* to receive approval,
  you can be more effective.

- Q: Stress is physical as well as mental.
- A: Yes. Sometimes the reboot can be physical.

- Q: Maybe teamwork can be part of the solution for all three types. If you do
  not trust your teammates to do something, then train them to do it. If you
  are not challenged, are keep doing the same things, then train someone else
  to do them.
- A: Good point.
