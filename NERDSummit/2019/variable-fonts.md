# Dynamic Typographic Systems and Variable Fonts: Scalable, Fast, and Fabulous

- Jason Pamental, web typographer @jpamental

- https://rwt.io
- https://rwt.io/newsletter
- https://noti.st/jpamental/THIjDV

## An odyssey of sorts

a bit of backstory

Jason left Drupal and agency work last June, and has been focusing on
typography (and walking his collies) and sometimes even getting paid for it.

Starting in 2009 (?) with TypeKit, web fonts could do a lot of traditional
typography: ligatures, make first line bold, etc.

## A new chapter

a fourth episode, if you will (A new hope?)

1. CSS custom properties
2. CSS calculations
3. CSS variables
4. Variable fonts
