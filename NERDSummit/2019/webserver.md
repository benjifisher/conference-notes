# How your webserver processes requests

- Oleksandr Trotsenko

- Drupal, Gentoo Linux
- FATV (chatbots for schools)
- co-maintainer of Webform, Webform Views

How do web servers handle concurrent requests?

## Concurrency

- ability of different parts of an algorithm to be executed out of order
  without affecting the outcome (paraphrase of Wikipedia)

When A and B must be done simultaneously?

Computer: do A very quickly, then B very quickly.

Human: do them simultaneously

## Apache (computer way)

- MPM: multi-processing module
- prefork (99%): so let's assume this one
- swarm of Apache processes
- controlled by Apache master process

## Nginx (human way: event loop)

- single unit of processing
- reacts (nearly) instantaneously to events

## Consequences

- CPU efficiency: event loop (nginx model) is better
- cost of process crash: Apache (prefork) wins

## PHP consequences (real world)

- PHP is not event-loop friendly
- Apache can execute PHP within its worker process
    - the burden of PHP is carried by all requests
- Nginx must outsource PHP evaluation to a third party (e.g., PHP-FPM).
    - Nginx incurs extra csts for PHP requests
- Increasing keep-alive is expensive in Apache

## Conclusions

- Use your brain. :)
