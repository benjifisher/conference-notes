# What is Kubernetes, and why should I care?

- Geri Jennings @izgerij

CyberArc (sp?) - Newton

If you can containerize your dev and local environments, what do you do when
you are ready to go to prod?

## Slides for this talk

https://bit.ly/2SR27Ws

## Containers: A Star is Born

Docker came out in 2014. It became the standard for containers.

There are other container runtimes now; active competition.

See Open Container Initiative.

## Under the hood

VM vs. Docker (Linux) vs. Docker (not Linux)

## How to define a container?

A: You actually define an image, a "snapshot" of a container.

You can *run* an image.

A Dockerfile defines an image.

```
docker build ... && docker run ...
```

## Why use containers?

- Consistent dev environment
- Consistent test environment
- Consistent production environment

## Moving to production

Start with containerized dev and test environments.

Deploy containers: you need infrastructure that

- understands how to run containers
- can orchestrate, network containers
- has options for monitoring

## Enter Kubernetes

- existing application
- minikube (local Kubernetes cluster)
- ...

## Key Kubernetes concepts

- Pod: smallest deployable unit, one or more containers (corresponding to a
  microservice)
- Services in a pod can share volumes and communicate via localhost
- In order to scale, run multiple pods.
- A **controller** creates and manages multiple pods.
- Each pod runs in a namespace, managed by the controller.

## Managing secrets

Three options:

1. Build into the application image
2. Store in Kubernetes secrets (native component, unencrypted)
3. Store in encrypted Kubernetes secrets
4. Store in a vault that works well with Kubernetes.

For demo, use (2) even though it is a bad idea in real life.

## Writing a manifest

... walk through some YAML file ...

- make sure there is at least one pod
- define the environment
- expose a port to the "outside world"

## Demo is nice, but what about production?

At scale, you need

- system to manage networking between microservices
- centralized monitoring and logging
- how to handle persistent data and security
- there are many options ... maybe too many

## Warnings

- Kubernetes is still evolving ... moving from early adopters to majority
  users
- It is still complex.
- There are not enough end-user stories/tutorials.
- Kubernetes focuses on the operator experience, but Faas and PaaS are better
  for the developer experience.
- YAML overload

## Managed Kubernetes options

- Google 8/2015
- AWS 6/5/2018
- Azure 6/13/2018
- Digital Ocean 12/2018

## Serverless and PaaS options based on Kubernetes

- Cloud Foundry
- OpenShift
- kubeless
- Fission (framework for serverless functions)

There are even VMs deployed on Kubernetes.

## Recall benefits of containers

- easy to build repeatably
- can run isolated or networked
- portable across clouds and OSs
- light weight
- based on immutable images, so rollback is quick and easy
- consistent dev/test/prod

## Try at home

awesome-kubernetes on GH
kubernetes.io
Kubernetes the hard way
github.com/izgeri/twitterSearchDemo

## CNCF Trail Map

How to get cloud native

## Q&A

Q: What about persistent data, e.g. databases?
A: We are working on it.

Q: Which should I use, container or VM?
A: It depends. For a big organization with lots of microservices, Kubernetes
   is a good fit.

Q: Say more about the dev experience.
A: One opinion is to keep using `docker compose` for dev. There may be tools
   that generate a Kubernetes manifest from a Docker compose file.
