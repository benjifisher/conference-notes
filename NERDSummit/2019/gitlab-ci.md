# GitLab CI/CD

https://bit.ly/nerds19-gitlab

- @petejohanson

## Who is this guy?

- remote dev/manager at Crossover Health
- has tried most CI systems

## Traditional CI systems

- Hudson/Jenkins
- TeamCity
- others

## Next Wave

- Travis
- Circle
- GitLab

## What is CI?

The practice of frequently integrating new or changed code with an existing
code repository.

(Wikipedia)

Q: How do you do that with confidence?

A: Practice

Make things routine and regular and easy.

Automation is a tool for this goal, not an end in itself.

## Example tasks

- compiling code
- running tests
- generating documentation
- building installers

## Continuous Delivery (CD)

continuousdelivery.com: ability to get changes of all types ... into
production or into the hands of users, safely and quickly, in a sustainable
way.

## Example tasks

- publishing to (mobile) app stores
- performing/updating Kubernetes deployments
- publishing libraries to repos/registries (e.g., NPM)
- releasing other executables

## GitLab CI/CD

- core part of GitLab
- also works with GitHub
- configure with YAML (pipelines and jobs)
- Docker or machine "executors"
- open source

## Building Blocks

- jobs
- pipelines

## Hello World

peterjohanson/nerdsummit-gitlab-ci-cd-hello-world

(simple job, one command)

## Stages

- break tasks into logical jobs
- different docker images, polyglot pipelines
- multiple jobs per stage

## Example

petejohanson/rust-gitlab-ci-cd-example

Two stages: build, test

Only move on to test stage if the build stage succeeds.

## Example: Test Task

- test report (JSON)
- code coverage report
- screenshots or videos

What do we do with these artifacts?

- downstream tasks may need these files
- store for download
- failure analysis
- deployment

## Produce artifacts

Add an `artifacts` key to a job. Specify expiration (optional) and paths.

## Consume artifacts

Declare a dependency, and GitLab makes the related artifacts available.
(If you do not specify dependencies, then you get artifacts from all previous
jobs.)

## Artifacts are downloadable.

## Docs

- artifacts
- dependencies

## Selective branches/tags

You can specify jobs, pipelines for which jobs or pipelines run.

Real-world example ...

## Environment variables

- built in
- declare new ones in YAML
- define in the repository configuration (can be protected)

Configure GitLab so that only maintainer can push tags to the repo, then use
tags to trigger stuff using these private variables.

## Caching

- avoid repeated work
- mostly for immutable, static-ish content
- useful for third-party dependencies (e.g., node modules)
- example ...

## Services

- run additional Docker images
- e.g., additional testing dependencies, like databases
- example ...

Docker-in-Docker is possible (`docker:dind` service)

## Templating

Manage lots of similar jobs.

E.g., build for different platforms.

You can also do it with pure YAML, using YAML "anchors".

## GitLab Pages

- just a specially named job
- leverage all other features
- specify output directory containing HTML

example: https://gitlab.com/petejohanson/hubris

## Non-Linux/Docker Runners

- macOS
- Windows
- Alternative architectures
- FreeBSD

DIY to some extent ... define these services ...

## Environments

- dev, test, stage, prod ...
- dynamically create environments

## Q&A

Q: What sets it apart from CircleCI?
A: They are similar. CircleCI is second favorite. Pipelines are simpler in
   GitLab. GitLab is OS and has CD.

Q: What services are available?
A: Any Docker image. You can pull it in from DockerHub (or elsewhere) even
   private repos.

Q: What is the caching system?
A: File-based, not HTTP.

Q: How do the caches get updated?
A: Each runner gets the cache from the central service.
