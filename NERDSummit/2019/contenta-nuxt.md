# Contenta CMS (Drupal API) + Nuxt.js

- Geoff St. Pierre, @serundeputy

Maybe add Lando to the title.

## Geoff

Tandem, Lando, Backdrop

## Outline

- Lando to manage local dev
    - drush
    - composer
    - yarn
    nuxt cli
- ContentaCMS: Drupal-based back end
     - jsonapi
     - [Decoupled Router](https://www.drupal.org/project/decoupled_router)
- Nuxt HS as Vue frontend
    - routes
    - asyncData()
    - Axios

## Repos

- https://github.com/thinktandem/mynuxt
- https://github.com/thinktandem/myapi

## Lando files

- myapi: Drupal recipe
- mynuxt: custom, based on a node service

## Look at the code

## Q&A

One thing we learned by experimentation is that JSON:API provides the raw text
field and also the filtered text (`value` and `processed` keys).
