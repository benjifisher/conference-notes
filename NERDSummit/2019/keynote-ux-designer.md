# Keynote: It's a great time to be a UX designer

- Jared Spool: @jmspool, jspool@uie.com, https://uie.com

## Kunming, China

- modern city
- old monasteries
- fake Apple stores (41 of them): "Apple Stoer"

There are also 72 imitation Apple stores in N. America, run by Microsoft. ;)

Why does everyone want to look like an Apple store?

Look at the numbers:

- about same annual # of visitors as Disney theme parks
- $341/$3017/$6050 per sq. ft.: average mall, Tiffany, Apple

A lot of thought has gone into every detail of display, purchase, ...
Everything is intentional.

## Design

The rendering of intent

Apple sued Samsung and won $1B.
Largest verdict ever.
What convinced the jury was a timeline of what the phones looked like over the
years: the Samsung phones looked more and more like iPhones.
There was a 128-page manual for QA dept. describing all features of Samsung
phones that do not look like iPhones as bugs.

## Intention

Imitation <--> Innovation

Imitation is cheaper and less risky. Design is not valued, viewed as a
commodity.

Innovation-based organizations see design as strategic: highly valued, built
into processes.

Business wins when it is intentionally innovative.
Ergo, business wins when it values designers.

## Classified ads

Craig's List killed the business for newspapers.
It ain't pretty, but it is effective and easy to use.

Then AirBNB came along and stole one category from Craig's List.

## Other disruptors

Square disrupted the credit-card industry.

Zipcar  disrupted the rental-car industry.

Cirque de Soleil went into the circus business when it looked like that
business was dying. They made several smart design decisions:

- cut out the animals
- market to adults, with prices $120-$200/seat
- they make more $$ each day than all of NYC's Broadway shows combined

## Milton Glazer (a founder of the design community)

> I move things around until they look right.

That is a very visual-centric view of design.

Great business models are intentionally designed. This is not specifically
visual.

## Customer journey for a defective product

Apple decided to give people appointments for repair. Then people get to the
store and browse while waiting for the appointment.

Apple did not invent the appointment. They added new value by applying it to a
new context.

Designing for experience means maximizing the experience over time in the
customer journey.

## Filing taxes

TurboTax created an app that used a smart phone and OCR to read W-2 and 1099,
then file taxes (1040-EZ or 1040-A) in 15 minutes.

## Cancer treatment

Pediatric cancer patients have to go into a machine for a scan and sit still
for 7.5 min.

With a little paint on the machine and pirate suits for everyone, the machine
looks like an adventure.

The flying mermaids only come out if the child sits perfectly still.

It used to be that 80% of pediatric patients were sedated. Now it is 0.01%.

## The birth of a great design

When Facebook bought Insagram, they got 13 new designers.

There are now more than 21K job listings for UX designers in the US.

## The rise of the UX generalist

Consider surgeons: first become a doctor, then learn surgery, then specialize,
e.g. in orthopedic surgery.

Anyone can become a UX generalist.
Learn all the skills needed to be a good experience designer.

Five steps:

1. Train yourself in one of the skills employers are looking for.
2. Practice your new skills.
3. Deconstruct as many designs as you can.
4. Seek out feedback. Listen to it.
5. Teach others.

... Experience design is about filling in the gaps.
