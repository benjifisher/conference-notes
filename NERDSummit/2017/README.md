# NERD Summit 2017

- [Community Charging Solutions](charging_stations.md)
- [Drupal 8:  Security Tips and Tricks](d8security.md)
- [OWASP Top 10: Hacking Web Applications with Burp Suite](owasp10_burp.md)
- [Real World Drupal 8 configuration management](d8_config_management.md)
- [Static ain't what it used to be: Everything old is new again with Jekyll.](jekyll.md)
- [The Benefits of Contributing](contrib_first.md)
- [True Tales of Python at Web Scale](python_web_scale.md)
- [TypeScript Deep Dive](typescript.md)
- [Web Performance](web_performance.md)
