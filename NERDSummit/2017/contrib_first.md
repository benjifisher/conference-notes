# The Benefits of Contributing
Damien McKenna

Mediacurrent has a "contrib first" policy.

If you depend on open source, then open source depends on you.

## 9 common problems

1. Need a new PM standard
2. Need a new design standard
3. Need a custom module - completely reusable, not client-specific ... more
   code to maintain.
4. Module hacked
5. Patch later
6. Unnecessary code
7. Duplicate efforts ... wasted effort
   1. Custom formatting
   2. Custom data import`
8. Tragedy of the Commons
9. Not Invented Here syndrome

Creating problems:

- technical debt
- needless duplication
- more domain-specific knowledge
- higher maintenance costs
- employee dissatisfaction

## Employers vs. employees

Employer Goals:

1. Maintain stability (systems, infrastructure, business)
2. Improve productivity (motivated staff do better work)

Employee Goals:

1. Work satisfaction
2. Self determination

See Dan Pink's TED talk on motivation.
http://www.ted.com/talks/dan_pink_on_motivation

- Extrinsic rewards
- Intrinsic (promote job satisfaction)

Extrinsic rewards level off in their effectiveness.

Primary instrinsic motivaters (and how they benefit employers and employees):

- Autonomy (might discover new opportunities - keeps work interesting)
- Mastery (Employees get more skilled, do better work - more satisfaction)
- Purpose

Contributing to Open Source hits all these rewards.

Autonomy motivator = "internal" time

## Contrib(ute) First

- Work with existing stuff (avoid duplication)
- seek to improve, not replace
- look for existing solutions

Why?  Collaboration!

- Work together (as widely as possible)
- Improve together
- Fix together
- Reduce technical debt

OSS = Democratic software.

Collaboration is more than code

## Mantras

1. Can this be reused?  Code, documentation, processes
2. Can this be improved?
3. Don't postpone.  (search, post questions, contributing back)

## Open Source as an investment

- staffing
- infrastructure
- future

## How much?

Compare with saving for retirement.  10% is a good start.

1. Promote a Contrib First mantra
2. Allow staff to contribute
3. ... on company time

For employees:

1. Always contrib first
2. Request contrib time
3. Personal time is yours

"The cavalry is not comming" - previous DrupalCon presentation?

## MediaCurrent's contrib committee

Started 1/2015
Track progress
Great for PR
Advance Drupal ecosystem

Goals:

1. Promote Contrib First
2. Train, mentor, encourage
3. Track progress
4. Promote existing work

Successes:

- Increased internal focus
- Increased Drupal contributions
- 26 monthly blog posts
- Increased sales
- PR on d.o Marketplace page (https://www.drupal.org/drupal-services)

## Start your own Contrib Committee

## More info

"Roads and Bridges" study from Ford Foundation
https://www.fordfoundation.org/library/reports-and-studies/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure/

"A Tale of two developers" (webchick)
http://webchick.net/embrace-the-chaos

"How to be a good Open Source citizen"

"Open Source and You"

"Codes of Conduct 101 - FAQ"

"Ethics of Unpaid Labor and the OSS Community"

https://modelviewculture.com/
