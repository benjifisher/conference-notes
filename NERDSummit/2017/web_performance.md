# Web Performance
Your web site on the fly in a cloud environment

Why Cloud?
- complete control
- ability to scale dynamically
- geographic distribution
- Only pay for what you use

How to make your website fly

- Reduce load time
- Reduce HTTP/HTTPS requests
  - Aggregate and compress CSS and JS assets
- Use APC (or Opcache ...)
