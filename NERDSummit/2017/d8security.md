# Drupal 8:  Security Tips and Tricks

Adam Bergstein (Nerdstein):  CivicActions

## Intro

D8 is only 1.5 years old.  Imagine its potential.

Security is hard.  It changes all the time.  You have to secure everything.

It is esp. hard to improve security without impacting usability.

Understand the problem you are trying to solve.  Don't just install every
security module.

## D8 Core

Many new features that affect security.

### Twig

- auto escaping
- no custom PHP in templates

### PHP Input Filter is gone.

### Trusted hosts

- associate Drupal with a specific domain.
- enables mediated domain checks to prevent spoofing

### New development constructs

- Plugin systems:  modules can implement their own extensibility
  (How is this fundamentally different from the hook system?)
- Composer:  modules can tap into many more OSS projects, including security
  tools.

See Peter W's talk from Drupal North camp.

## Contrib:  Hardening techniques

Lock down specific attack vectors.
Limit vulnerabilities by ...
Make the system more idiot-proof.

- Autologout (configure re-authentication timeouts per role)
- Session Limit (do not allow logins from multiple devices)
- Login security (restrict authentication based on IP address;
  configure the cap on unsuccessful login attempts)
- Seckit (many ways to configure response headers)
- Honeypot and Captcha

## Auditing

Forensics:  if you get hacked, you need data.
Auditing tools can give you ongoing data.

- Use `syslog`, not `dblog`.
- Evaluate all aspects of the system to decide what should be logged.
- Log as much as possible.

### Modules

- Site Audit (drush/Drupal module) - use it and save results over time.
- Security Review module (Nerdstein is working on a plugin system.)
- Login History`

## Authentication

Make it harder to log in.
Balance inconvenience against security and cost.

- TFA module (It provides an API, so it is extensibile.)
- Simple SAML PHP Auth (SSO)
- Password Policy (Nerdstein says it was hard to port to D8.)
- Password Strength (smart measurement of password strength)

## Encryption

Look at both data at rest and communication channels.

- Infrastructure first (the lower the better)
- Use full HTTPS/TLS for all web traffic.
- Consider full disk encryption from your hosting provider.

Modules:

- Key module (like Apple Keychain.app)
- Encrypt (provides API for encryption/decryption of data in database)
  - Many modules rely on it.
  - Multiple algorithms for encryption/decryption:  RealAES, Diffuse
  - Field Encrypt, File Encrypt
  - Pubkey Encrypt

## Devops

Your processes are critical for ensuring that you consistently use good
security processes.

- Use code repos and PRs.
- Perform code review and pay attention to custom code.
- Tag releases to isolate changes.
- Update your (contrib) code regularly.
- Automation helps avoid manual mistakes.
- Drush helps script automation.

- Coder module (PHPCS)
- Hacked
- Backup and Migrate
  (Me:  Do not just backup:  test restoring from backup!)

## Environment

Secure your infrastructure.

- Use dev/stage/prod workflow.
- Use a CDN as a web application firewall (WAF)
- Use Splunk/ELK to analyze logs.
- Use cloud-based environments.  There are many security tools and arch.
  advantages.
- Have a failover environment.
- Test your backup and failover plans.

## Acknowledgements

- CivicActions
- Drupal Community
- NERD Summit
- Peter Wolanin:  D8 Security presentation
