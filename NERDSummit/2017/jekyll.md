# Static ain't what it used to be: Everything old is new again with Jekyll.

Jim Fisk

Case study of The City of Boston's budget implementation using Jekyll.

What is Jekyll?

- static site generator
- Ruby gem
- Liquid template engine
- Content in markdown
- YAML front matter

Advantages:

- Free hosting
- Secure
- Fast page loads
- Easy to use

Disadvantages:

- Admin experience is editing files in markdown.

## budget.boston.com

- tight timeline
- get JSON from other services
- Cabinet > Department > Program

Set up site wrapper with `gulp`

https://www.boston.gov/api/v1/layouts/generic

download and build with js script.

local vs. prod config ... include different config.yml files.
One of the variables is `site.baseurl`.

### Components

- configurable layouts with markdown


https://github.com/CityOfBoston/budget.boston.gov
https://github.com/jantcu/budget.boston.gov

### JSON components

same effect as Drupal content types/templates

### Google Forms/Sheets backend and API

Jim needs a Chrome plugin for formatting JSON.
He needs to use `ci"`.

There is a hack to handle multiple elements of the same type.  Maybe
restructure the YAML to have a list of components instead of a keyed array.
Gloassary of terms (!).

### Use ChartJS for pretty formatting of charts.

### Jekyll Admin and Prose.io


https://github.com/jekyll/jekyll-admin
http://prose.io/
https://github.com/prose/prose
