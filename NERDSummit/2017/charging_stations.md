# Community Charging Solutions

https://www.drupal.org/u/dbt102
(He got a Ph D for writing a Drupal module!)

Clipper Creek is the only charging station that is BSCnet comptible.
BAC = Building Automation COntrol (?)

Brevard College ... case study ... already has a Drupal site, try to get them
to install charging stations and post availability on their web site.
(Brevard is already clean-energy minded.)

Take away:  if your customer has a Drupal site, encourage them to install
charging stations in their buildings.

Tesla:  "All our patent are belong to you."

Carnegie Mellon study:  if you give people information and control over how
much energy they are using, then they will save energy.
Even those who just have info (no control) save energy, about 7%.  Maybe we
can expect similar savings just by providing the info on a web site.
(doubtful)

35% of the electricity that is generated is never used.  It is important not
to increase our peak power demands ... and this is where smart charging
stations, connected to BAC, can help.

It costs 2 cents/minute to keep the owner and passenger of a $100K car (Tesla)
on your property.

Tesla uses Drupal for its web site and its in-car dashboard.
