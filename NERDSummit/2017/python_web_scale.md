# True Tales of Python at Web Scale

Paul Bissex

https://github.com/pbx/school-base

The 2-minute bio is at 15 minutes and counting.

## Analytics and caching

Request:  record each image flip in galleries (sliders)

Snowpocalypse:  2" of snow in Atlanta
Drudge Report linked to a photo gallery on Cox Media
Cox Media wasn't caching comments on photo galleries ...

## Automated testing

## Code Review

sublime linter plugin for Sublime Text.
reviewboard is a service for code reviews.
https://www.reviewboard.org/

## Hire Right

## Learn from mistakes
