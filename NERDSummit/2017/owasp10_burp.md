# OWASP Top 10: Hacking Web Applications with Burp Suite

Chad Furman (https://github.com/chadfurman ?)

BeEF - Browser exploitation framework

[Burp suite](https://portswigger.net/burp/)

http://sqlmap.org/ "Automatic SQL injection and database takeover tool"

Mutillidae:  born to be hacked
http://www.irongeek.com/i.php?page=mutillidae/mutillidae-deliberately-vulnerable-php-owasp-top-10
or
https://www.owasp.org/index.php/OWASP_Mutillidae_2_Project ?

## SQL Injection

Lots of examples of how to use SQL Injection vulnerability if you can find
one.
You can usually get shell access this way.

You can typically log in as admin, esp. if it has uid = 1.

https://chads.website/
https://chads.website/general%20news/2017/03/19/OWASP-Top-10-Hacking-with-Burpsuite.html
