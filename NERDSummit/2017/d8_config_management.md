# Real World Drupal 8 configuration management

Lanette Miller, Ally Gonthier

a.k.a. Ally and Lanette's Excellent Configuration

- Who we are
- D8 config basics (quick)
- workflows
- overriding
- There's a module for that

## Who we are

- Lanette
- Ally

They look at broken Drupal sites all day long.

## D8 config system

- The site, not the module, owns the configuration.
  This makes it complicated for modules that want to update config.
- Configuration can be overridden (e.g., dev/stage/prod)

Lanette should know how to set up git between two directories on the local
system.

## Real world management

- Use `vcs` directory on dev and prod.
- Use `sync` directory on prod, under `sites/default/files`.
- Use a diff tool to compare exported config from dev and prod.
- Use `drush cim` to import from the working (merge) directory and `drush cex`
  to export to the `vcs` directory.  Git add and commit and push.
- Push to prod and `drush cim` there.

## Overriding Configuration

- Use `drush config-list` (short form `cli`) ... Does Lanette know how to make
  a drush alias?
- Then `drush config-get` and `drush config-set` (`cget` and `cset`).
- Use the `$config` array in `settings.php`.

## Modules

- Features (`features`)
- Config Update (`config_update`)
- Config Sync (`config_sync`)
- Config Read Only (`config_readonly`)
- Menu Link Config (`menu_link_config`)

## Gotcha!

Custom blocks cannot be properly exported and imported.
https://www.drupal.org/node/2756331


bit.ly/configrealworld
