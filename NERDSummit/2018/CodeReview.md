# How to Do Code Reviews Like a Human

Michael Lynch

## Popular advice about code reviews

- What isn't covered?
  - How is the author going to react to a list of defects?
  - How do you help the author learn from their mistakes?

## My assumptions

- More than bugs; social, learning
- Teammates are human, with emotions
  - They learn in ways that humans do.

## Definitions: what is a code review?

- changelist
- asynchronous
- ends when reviewer give approval

## 1. Settle style arguments with a style guide.

## 2. Let computers do the boring parts.

- Use linters and formatters as much as possible.
- Use CI to compile code and run tests.
- Reason: developer time is scarce. Focus is even more scarce.

## 3. Be generous with code examples.

- Developer might be under pressure to get code checked in.
- Examples demonstrate that you want to help.

## 4. Never say "you" (controversial advice)

- Focus on the code, not the coder. Use the passive voice or say "we".
- "You" brings ego into the discussion.

## 5. Frame feedback as requests, not commands

- Be more polite in code reviews than you would in normal speech.
- Requests make it easier for the author to push back politely. (Sometimes the
  original author is right.)

## 6. Offer sincere praise

- Code reviews don't have to be about mistakes.
- Keep an eye out for things that delight you.
- Recognize when they show improvement.

## 7. Aim to bring the code up a letter grade or two

If you think in terms of A, B, C, D, F, do not always aim for A. If it started
at a D, settle for B or C.

- Developer patience is finite.
- Stress increases with the length of the process.

## 8. Handle stalemates proactively

Signs:

- Tone is growing tense or hostile.
- Notes per round is not trending downward.
- You are getting a lot of pushback.

Remediation:

- Talk it out.
- Design review (with the larger team)
- Last options: concede or escalate.

Recovering:

- Talk to your manager.
- Take a break from each other.
- Study conflict resolution.

## Final thoughts

- There is no single Right Way to do code reviews.
- It depends on the team and person.
- Think during reviews about
  - Where is tension coming from?
  - Is code quality reaching the right level?
  - Learn good and bad things from those who review you.

Blog post: https://mtlynch.io/human-code-reviews-1/
michael@mtlynch.io
Twitter @deliberatecoder
