# Intro to GraphQL (with Relay Modern and Postgraphile)

Ship Faster. Avoid Tech Debt. Finish on time.

Chad Furman

## Plug

Chad works for Clevertech, a global, distributed company.

## REST vs. GraphQL

- many endpoints vs. just one

Send a POST request to the endpoint with data that looks sort of like JSON
without commas, and is strictly typed.

GraphQL generates the documentation automatically from the field schema.


- declarative data dependencies
- manage deprecated fields with a simple flag
- automatically model a Postgres database as an API [Postgraphile](https://www.graphile.org/postgraphile/)

## Migrations (not sure what he means by this)

http://sqitch.org/

> Sqitch is a database change management application.

[Relay modern](https://facebook.github.io/relay/)

[RPG Boilerplate](https://github.com/chadfurman/rpg-boilerplate)
> Relay (React), Postgres, and Graphile (GraphQL): A Modern Frontend and API Boilerplate

- This is a FOSS, alpha version of what Chad is using in productions.
- The production version is handling sensitive data for international clients
  using row-level security (RLS) and RBAC (role-based access control).
