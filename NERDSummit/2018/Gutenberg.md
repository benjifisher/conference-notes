# Getting Going with Gutenberg

Amanda Giles

http://amandagiles.com/nerd2018

For now, Gutenberg is the site editing tool for WP, but there is a framework
behind it that will be used in other parts of WP going forward.

Goal: make the editor more WYSIWYG and expose embeds (FB, Youtube, etc.)

- it includes several standard edit blocks
- accessibility warnings
- saveable snippets
- access to embeds

It is currently available as a plugin, but is still under development.

It may be merged into WP in April 2018, then part of WP 5.0 when it is
released.

There will be a "classic" editor available as a plugin (maybe not forever).

The back end is still the same: Gutenberg "blocks" are stored in the HTML text
field enclosed in comments.

Plugin/theme developers will be able to add additional blocks for use with
Gutenberg.

Gutenberg Blocks

- built (mostly?) in JS using React (and a custom API)
- Use ES5 (2009) or ESNext - currently ES6 (2015)
  - ES5 is native, and there is a compatibility layer/compiler for more recent
    versions.
- Gutenberg supports Javascript XML (JSX)

JS libraries

- wp.blocks
- wp.element
- wp.components (buttons etc.)
- wp.i18n
- wp.date

The plugin/theme author is responsible for making sure that the block looks
the same in the editor as it does on the front end of the site.
