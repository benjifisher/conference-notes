# NERD Summit 2018

- [Docker for Development](Docker.md)
- [Getting Going with Gutenberg](Gutenberg.md)
- [How to Do Code Reviews Like a Human](CodeReview.md)
- [Intro to GraphQL (with Relay Modern and Postgraphile)](GraphQL.md)
- [It's Not Your Parents' HTTP](HTTP.md)
- [Quantum Computing](QuantumComputing.md)
