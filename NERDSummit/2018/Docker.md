# Docker for Development

Geri Jennings

Vocbulary:

- Docker image
- Container
- Docker registry

Benefits

- standardized dev environement
  - not just dev: stage, prod as well
- manage dependencies
- easier to update prod
- good documentation

N.B. The slides show commands that use
[Summon](https://www.cyberark.com/blog/introducing-summon-get-your-secrets-into-source-control/)
to handle API keys.
