# It's Not Your Parents' HTTP

Gleb Bahmutov

- @bahmutov
- glebbahmutov.com
- github.com/bahmutov
- slides.com/bahmutov

https://www.cypress.io test runner (free, OS) for web sites

March 12, 1989: http://info.cern.ch/Proposal.html

1991: Edit and view HTML documents

first web site: info.cern.ch

Also 1991: HTTP/0.9 (650 words)

- only GET: no sessions, cookies, binaries
- no error codes
- relies on TCP/IP

HTTP performance is ied to TCP properties.

TCP:

1. guaranteed data delivery
2. guaranteed packet order

HTTP/1.0 (1996)

- 60 pages
- non-HTML data (images)
- status codes
- user preferences

HTTP/1.1

- Formalize best practices
- 27 drafts, latest 2014
- performance
- security
- usability

_The Tangled Web_ (recommended reading)

HTTP Request and Response

headers ...

- state (cookies)
- cache control
- security
- 10's or 100's of requests for a single page

obesity: avg. page size is 2 MB.

AJAX: from web sites to web apps (avoid full page reload)

1996: IE introduced iframe for this.

XMLHTttpRequest - gets data from server via client-side script

Fetch: new standard

HTTP/1.1 today:


REST	GraphQL
HTTP
TCP
IP

Not the only game in town any more

- HTTP
- Web socket
- Service worker

HTTP is dead. Long live HTTPS!

HTTPS is expensive: extra round trips for TCP + TLS handshakes

Service worker blurs the line between client and server.

What if an attacker can load a malicious Service worker?

HTTP/2: making the web faster

https://hpbn.co/

HTTP/2 Server push: I know what you want.
Problem: server pushes resources even if they are already in the browser's
cache.

Main features of HTTP/2:

1. multiplexing
2. compression
3. flow control
4. resource push

Main problem: if a packet is lost, then everything else is delayed. (Because
it runs on top of TCP.)

QUIC is an alternative to HTTP/2 (H2) that uses UDP instead of TCP.

Secret: many Google properties use QUIC with Chrome already.

slides.com/bahmutov/http-nerd-summit
