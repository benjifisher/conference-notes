# Quantum Computing

Jerry Dixon

1821 Don Juan, written by Lord Byron, the first "rock star"
Ada Lovelace was his daughter.

1828 Charles Babbage builds the difference engine. (It was already designed.)
He started to work on the Analytical Engine, size of a locomotive.
Ada Lovelace helped home translate reviews from Italian.
She wrote a program for the AE that computed Bernoulli numbers.
She said that the AE could never do anything original (my poor paraphrase).

Alan Turing: saved 14M lives by cracking the Enigma code.
He invented the Turing test.

1958 integrated circuit (7/16")

1965 Gordon Moore came up with Moore's Law (_Electronics_ Magazine)
The number of transistors per chip will double every two years.

1955 the term "Artificial Intelligence" was coined.

...

He predicts that 2018 will be the year of press releases in QC.

He claims that QC is way beyond the stage of research projects, and that we
will have real QCs in the near future.

E.g. 4 Qbits are in a superposition of 16 states, so somehow they represent
the same amount of data as 16 ordinary bits.

Based on that, 1000 Qbits represent an unimaginable amount of classical
computing power.

M$ has a language (Q#) for dealing with QC: the primitive are things like
"tunnel" and "entangle". M$ also has an SDK for simulating a QC. A 16GB laptop
can simulate (IIRC) 20 Qbits.
